#!/usr/bin/env python3.9
# -*- coding: utf-8 -*- 

import bpy
import bmesh
from bpy_extras.object_utils import world_to_camera_view
from mathutils import Vector, geometry
import itertools
from prj.drawing_subject import Drawing_subject, drawing_subjects
from prj.utils import add_modifier, apply_modifier, debug_here
from prj.mesh_utils import create_line_obj, join_meshes
from prj.drawing_style import drawing_styles

drawing_symbols = []

def tuple_to_str(tup, sep):
    return sep.join([str(i) for i in tup])

def get_same_collection_subjs(collection: bpy.types.Collection) \
        -> list[Drawing_subject]:
    """ Get all the subjects in collection (and in sub-collections too) """
    collection_subjects = []
    for obj in collection.all_objects:
        for subj in drawing_subjects:
            subj_condition = subj.obj_eval.name and \
                    subj.obj_eval.original == obj
            instancer_condition = subj.instancer_eval and \
                    subj.instancer_eval.original == obj
            if not subj_condition and not instancer_condition:
                continue
            collection_subjects.append(subj)
    return collection_subjects

class Drawing_symbol(Drawing_subject):
    parent_collection: bpy.types.Collection
    cam_distance: float
    collection_subjects: list[Drawing_subject] = []

    def __init__(self, **kwargs):
        if self in drawing_symbols:
            return
        super().__init__(**kwargs)
        ## TODO Make STAIRS_CUT a variable
        self.is_stairs_cut = self.symbol_type == 'STAIRS_CUT'
        if self.is_stairs_cut:
            self.back_stairs_obj = None
            ## Add back_drawing_style
            self.styles.append(drawing_styles['b'])
        parent_collection = [coll for coll in self.collections \
                if self.obj_eval.original in list(coll.objects)]
        self.parent_collection = parent_collection[0] if parent_collection else \
                self.working_scene.scene.collection
        drawing_symbols.append(self)

    def __repr__(self):
        return 'SYMBOL: ' + super().__repr__()

    def apply_stairs_cut(self) -> None:
        """ Create a volume based on stairs cut symbol and subtract it from the 
            stairs elements """
        SAFETY_OFFSET = .1

        scene: bpy.types.Scene = self.working_scene.scene
        draw_cam = self.drawing_camera

        ## Get cut symbol vertices view by camera
        obj_verts = [self.matrix @ v.co for v in self.obj.data.vertices]
        line_start = world_to_camera_view(scene, draw_cam.obj, obj_verts[0])
        line_end = world_to_camera_view(scene, draw_cam.obj, obj_verts[-1])

        self.cam_distance = max(line_start.z, line_end.z)
        ## Project frame verts to the extended cut line 
        ## and get the coords of the longest distance
        cam_frame = [v.resized(2) for v in draw_cam.normalized_frame]
        projected_frame_on_line = [geometry.intersect_point_line(f, 
            line_start.resized(2), line_end.resized(2))[0] for f in cam_frame]
        segments_couples: list[tuple[Vector]] = list(itertools.combinations(
            projected_frame_on_line, 2))
        verts_distance: int = 0
        frame_cut: tuple[Vector] = None
        for segments_couple in segments_couples:
            segments_distance = (segments_couple[1] - segments_couple[0]).length
            if segments_distance > verts_distance:
                verts_distance, frame_cut = segments_distance, segments_couple

        ## Put frame_cut verts in frame coordinates (camera position is the 
        ## origin) and create the actual cutting_obj (a line) at frame position
        cam_scale = draw_cam.ortho_scale
        factor_x = draw_cam.frame_factors['x']
        factor_y = draw_cam.frame_factors['y']

        frame_intersections = [Vector((cam_scale * factor_x * (v.x-1/2), 
            cam_scale * factor_y * (v.y-1/2), -draw_cam.clip_start)) \
                    for v in frame_cut]
        cutting_obj = create_line_obj('Stairs_obj_bool', frame_intersections)
        scene.collection.objects.link(cutting_obj)
        cutting_obj.matrix_world = draw_cam.matrix

        ## Extrude cutting_obj (from line to plane) from frame location 
        ## to symbol line location
        screw_offset = draw_cam.clip_start -self.cam_distance - SAFETY_OFFSET
        screw_mod = add_modifier(cutting_obj, 'Screw_extrude', 'SCREW', 
                {'angle': 0, 'steps': 1, 'screw_offset': screw_offset, 
                    'use_smooth_shade': False})
        
        ## Extrude the cutting_obj (now a plane) to make a solid
        cut_plane_verts = [
                cutting_obj.matrix_world @ cutting_obj.data.vertices[0].co, 
                cutting_obj.matrix_world @ cutting_obj.data.vertices[-1].co, 
                cutting_obj.matrix_world @ (cutting_obj.data.vertices[0].co + \
                        Vector((0,0,-1)))]
        # The side to keep is the one where the origin of cut symbol is
        distance_from_origin = geometry.distance_point_to_plane(
                self.obj.location, cut_plane_verts[0], 
                geometry.normal(cut_plane_verts))
        solidify_offset = round(-distance_from_origin/abs(distance_from_origin))
        # The length of solidify extrusion is the maximum distance in frame
        cam_frame_diagonal = (draw_cam.frame[0] - draw_cam.frame[2]).length
        solid_mod = add_modifier(cutting_obj, 'Solidify_extrude', 'SOLIDIFY', 
                {'thickness': cam_frame_diagonal, 'offset': -solidify_offset})

        ## Get all the subject contained in parent collection and sub-collections
        self.collection_subjects = get_same_collection_subjs(
                self.parent_collection)

        ## Create the actual drawn objects
        joining_objs = []
        top_vert_z = None
        for subj in self.collection_subjects:
            # Get the chance for setting stairs_cut_ref to interested subjects
            subj.set_stairs_cut(self)

            if subj.is_symbol:
                continue

            ## Get the higher vert of stairs (from camera point of view)
            for vert in subj.mesh_eval.vertices:
                vert_coord = subj.matrix @ vert.co
                vert = world_to_camera_view(scene, draw_cam.obj, vert_coord)
                if not top_vert_z:
                    top_vert_z = vert.z
                    continue
                if vert.z >= top_vert_z:
                    continue
                top_vert_z = vert.z
            self.top_vert_dist = top_vert_z
            
            ## Join all objects in collection and create self.back_stairs_obj 
            ## (it's the part of the stairs that is cut off)
            diff_mod = add_modifier(subj.obj, 'prj_stairs_cut_bool', 'BOOLEAN', 
                    {'object': cutting_obj, 'operation': 'DIFFERENCE', 
                        'solver': 'FAST'})
            eval_obj = apply_modifier(subj.obj, diff_mod)
            subj.obj.modifiers.remove(diff_mod)
            joining_objs.append(eval_obj)

            ## Cut the stairs parts to the actual drawn geometry 
            ## and replace subject data accordingly
            intersect_mod = add_modifier(subj.obj, 'prj_stairs_cut_bool', 
                    'BOOLEAN', {'object': cutting_obj, 'operation': 'INTERSECT',
                        'solver': 'FAST'})
            eval_obj = apply_modifier(subj.obj, intersect_mod)
            subj.obj.modifiers.remove(intersect_mod)
            subj.link_data(eval_obj.data)
            bpy.data.objects.remove(eval_obj)
            #debug_here('C:\\Users\\MarcoFerrara\\Documents\\raise.blend')

        self.back_stairs_obj = join_meshes(self.name + '_back_view', 
                joining_objs)
        scene.collection.objects.link(self.back_stairs_obj)
        self.back_stairs_obj.hide_viewport = True
        for joined_obj in joining_objs:
            bpy.data.meshes.remove(joined_obj.data)

        cutting_obj.hide_viewport = True

