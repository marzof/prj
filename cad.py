#!/usr/bin/env python3.9
# -*- coding: utf-8 -*- 

# Copyright (c) 2021 Marco Ferrara

# License:
# GNU GPL License
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import bpy
from prj.drawing_camera import get_drawing_camera
from prj.working_scene import get_working_scene
from prj.utils import get_drawing_styles, debug_here, UNITS, UNITS_SYSTEM

HATCH_LAYER_NAME = 'htc'
HATCH_LAYER_COLOR = 250
DEFAULT_LAYER_COLOR = 7
DEFAULT_LAYER_LTYPE = 'CONTINUOUS'
LINETYPE_BASE = 2.54/10.
LINETYPES = [
        ('DOTTED', 'Dotted . . . . . . . . . . . . . . . . . . . . . .', 
            [LINETYPE_BASE, 0.0, -LINETYPE_BASE/2.]),
        ('HIDDEN', 'Hidden _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _',
            [LINETYPE_BASE, LINETYPE_BASE/2., -LINETYPE_BASE/4.]),
        ('DASHED', 'Dashed __  __  __  __  __  __  __  __  __  __  __  __',
            [LINETYPE_BASE, LINETYPE_BASE, -LINETYPE_BASE/2.]),
        ('DASHDOT', 'DashDot __  . __  . __  . __  . __  . __  . __  .',
            [LINETYPE_BASE, LINETYPE_BASE, -LINETYPE_BASE/2., 0.0, 
                -LINETYPE_BASE/2.]),
    ]

def get_default_lineweight() -> int:
    import ezdxf
    return ezdxf.lldxf.const.LINEWEIGHT_DEFAULT

def get_scale_factor() -> float:
    """ Get the scaling factor (i.e for a 1:20 scale, factor is 20) considering 
        the scale drawing and the default unit for svgs 
        (millimeters for metric, inches for imperial """
    cam = get_drawing_camera()
    working_scene = get_working_scene()
    settings = cam.data.prj_drawing_settings
    units_system = working_scene.scene.unit_settings.system
    base_units = UNITS_SYSTEM[units_system]['svg_base_units']
    drawing_unit = working_scene.scene.unit_settings.length_unit
    factor = UNITS[base_units]/UNITS[drawing_unit]
    return factor / (settings.scale_numerator / settings.scale_denominator)

def get_simple_units(coords: tuple[float], drawing_scale_factor: float) \
        -> tuple[float]:
    """ Correct coords in simple units (no units of measurements) 
        by scaling x values and vertically mirroring y values """
    cam = get_drawing_camera()
    raw_resolution = get_working_scene().raw_resolution
    new_x = coords[0] * drawing_scale_factor
    new_y = 2 * raw_resolution[1] - (coords[1] * drawing_scale_factor)
    return new_x, new_y

def create_dxf_layer(dxf, style: 'Drawing_style' = None, name: str = None, 
        color: int = DEFAULT_LAYER_COLOR, linetype: str = DEFAULT_LAYER_LTYPE, 
        lineweight: int = None) -> 'ezdxf.entities.layer.Layer':
    """ Create a layer for dxf based on style or parameters """
    if style:
        layer = dxf.layers.new(name=style.name) 
        layer.dxf.color = style.dxf_color
        layer.dxf.linetype = style.dxf_linetype
        layer.dxf.lineweight = style.dxf_lineweight
        return layer
    layer = dxf.layers.new(name=name) 
    layer.dxf.color = color
    layer.dxf.linetype = linetype
    layer.dxf.lineweight = lineweight if lineweight \
            else get_default_lineweight()
    return layer

def create_dxf_linetypes(dxf: 'ezdxf.document.Drawing') -> None:
    """ Populate dxf by linetypes collected in LINETYPES """
    for name, desc, pattern in LINETYPES:
        if name in dxf.linetypes:
            continue
        linetype = dxf.linetypes.new(name=name, dxfattribs={
            'description':desc, 'pattern':pattern})

def generate_dxf(filepath: 'pathlib.Path') -> 'ezdxf.document.Drawing':
    """ Create dxf from filepath and populate it by obj_data """
    import ezdxf
    from ezdxf import zoom
    from prj.svg_data import svgs_data
    dxf = ezdxf.new('R2010')
    populate_dxf(dxf, svgs_data)
    zoom.extents(dxf.modelspace())
    dxf.saveas(filepath)
    return dxf

def create_polyline(container, coords: list[tuple[float]], layer: str = '0', 
        linetype: str = 'BYBLOCK', color: int = 0, lineweight: int = -2, 
        flags: int = 128) -> 'ezdxf.entities.lwpolyline.LWPolyline':
    """ Create a lwpolyline with parameters in dxf """
    ## lineweight = -2 -> BYBLOCK ; flags = 128 -> PLINEGEN
    lwpoly = container.add_lwpolyline(coords, dxfattribs={
        "layer": layer, "linetype": linetype, "lineweight": lineweight,
        "color": color, "flags": flags})
    return lwpoly

def populate_dxf(dxf, obj_data: list['Svg_data']) -> None:
    """ Populate dxf by obj_data """
    import ezdxf
    drawing_styles = get_drawing_styles()
    drawing_scale_factor = get_scale_factor()

    ## Create linetypes and set global linetype scale factor
    create_dxf_linetypes(dxf)
    dxf.header['$LTSCALE'] = 10 * drawing_scale_factor

    ## Create hatch layer
    create_dxf_layer(dxf, name=HATCH_LAYER_NAME, color=HATCH_LAYER_COLOR)

    for obj in obj_data:
        subject = obj.subject
        for svg_read in obj.data:
            style_name = svg_read.data['style']
            block_name = subject.get_svg_id(style_name)
            block = dxf.blocks.new(name=block_name)

            ## Get the actual style and add related layer
            for d_style in drawing_styles:
                if drawing_styles[d_style].name == style_name:
                    actual_style = drawing_styles[d_style]
                    break
                elif drawing_styles[d_style].default:
                    actual_style = drawing_styles[d_style]
            if style_name not in dxf.layers:
                create_dxf_layer(dxf, actual_style)
        
            ## Convert units of measurements coords into simple coords
            if 'scaled_coords' not in svg_read.data:
                continue

            scaled_coords = svg_read.get_data('scaled_coords')
            if style_name == 'cut':
                hatch = dxf.modelspace().add_hatch(color=256, 
                        dxfattribs={"layer": HATCH_LAYER_NAME,
                            "hatch_style": ezdxf.const.HATCH_STYLE_NESTED,
                            })
                
            for i, sc_coords in enumerate(scaled_coords):
                no_measure_coords = [get_simple_units(coords, 
                    drawing_scale_factor) for coords in sc_coords]

                ## Create the polylines inside the block
                lwpoly = create_polyline(block, no_measure_coords)

                ## Add hatch to cut object
                if style_name != 'cut':
                    continue
                if subject.is_open_cut:
                    continue
                path = hatch.paths.add_polyline_path( 
                        lwpoly.get_points(format="xyb"), is_closed=True,
                        flags= ezdxf.const.BOUNDARY_PATH_DEFAULT)
                
            ## Add block instance to modelspace
            dxf.modelspace().add_blockref(block_name, (0.0, 0.0), dxfattribs={
                "layer": style_name, 'xscale': 1., 'yscale': 1., 'rotation': 0.})
