#!/usr/bin/env python3.9
# -*- coding: utf-8 -*- 

# Copyright (c) 2021 Marco Ferrara

# License:
# GNU GPL License
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import os
import site
import re
import bpy
import inspect
import time
import subprocess
import itertools
cwd = os.path.dirname(os.path.realpath(__file__))
site.addsitedir(os.path.join(cwd, "libs", "site", "packages"))

from prj.drawing_camera import get_drawing_camera
from prj.working_scene import get_working_scene
from prj.drawing_maker import tmp_svgs
from prj.drawing_subject import reset_subjects_list
from prj.selection_subjects import get_selected_subjects
from prj.subjects_processor import get_overlaps
from prj.svg_data import reset_svgs_data
from prj.utils import get_resolution, set_hi_res_percent
from prj.utils import debug_here, get_render_basepath, tmp_imgs
from prj.utils import remove_file, check_dir, get_addon_preferences
from prj.cad import generate_dxf
from prj.progress_indicator import get_progress_indicator
from prj.draw import draw_subjects, prepare_subjects
from prj.svg_generate import get_svgs_from_svg_data, get_svg_composition
from prj.prepare_drawing import process_args, get_subjects

RESOLUTION_FACTOR: float = 96.0 / 2.54 ## resolution / inch
main_progress_count = itertools.count()

def reset_scene() -> None:
    """ Remove all data created by prj """
    cam = get_drawing_camera()
    if cam:
        cam.remove()
    reset_svgs_data()
    reset_subjects_list()
    get_working_scene().remove(del_subjs=True, clear_objs=True)
    global main_progress_count
    main_progress_count = itertools.count()

def main(cam: bpy.types.Object = None, objs: list[bpy.types.Object] = None,
        context: bpy.types.Context = None) -> "pathlib.PosixPath":

    if inspect.stack()[1].function == '<module>':
        ## main() is launched by terminal (not by UI through __init__.py)
        print('Start now')
        start_time = time.time()

        context = bpy.context
        cam, objs = process_args()
        render_basepath = get_render_basepath()
        if not cam:
            error_message = "Just one camera has to be selected"
            print("\n", error_message)
            return
        if not check_dir(render_basepath):
            error_message = f'Can\'t find output path folder: {render_basepath}'
            print("\n", error_message)
            return

    ## Prepare progress indicator
    with open(__file__, 'r') as f:
        lines = f.read().split('\n')
    main_progresses = [line for line in lines \
            if re.search(r"next\(main_progress_count\)", line)]
    progress = get_progress_indicator()
    progress.set_phase(phase_data={'name': 'main', 'start':0., 'end':1., 
                'steps':len(main_progresses)})
    progress.update(phase_name='main', value=0.)

    resolutions = get_resolution(camera_data = cam.data)
    working_scene = get_working_scene(resolution = resolutions['resolution'],
            raw_resolution = resolutions['raw'],
            resolution_percentage = set_hi_res_percent(cam.data))
    drawing_camera = get_drawing_camera(cam) 
    linked = drawing_camera.linking_type == 'linked'
    progress.update(phase_name='main', step=next(main_progress_count))

    subjects = get_subjects()
    next(main_progress_count) ## -> get_subjects @ prepare_drawing
    next(main_progress_count) ## -> get_in_frame_subjects @ subject_getter

    get_overlaps(subjects)
    next(main_progress_count) ## -> get_overlaps @ subject_processor
    if not drawing_camera.disable_render:
        next(main_progress_count) ## -> render_separate_subjects 
                                  ##                @ subject_processor

    subjects_to_draw = get_selected_subjects(subjects, objs)
    context.window.scene = get_working_scene().scene

    prepare_subjects(subjects_to_draw)
    next(main_progress_count) ## -> prepare_subjects @ draw
    draw_subjects(subjects_to_draw)
    next(main_progress_count) ## -> draw_subjects @ draw
    
    get_svgs_from_svg_data(subjects_to_draw)
    next(main_progress_count) ## -> get_svgs_fromt_svg_data @ svg_generate

    if drawing_camera.export_svg and linked:
        get_svg_composition(subjects_to_draw, not bool(objs))
    progress.update(phase_name='main', step=next(main_progress_count))
    if drawing_camera.export_cad:
        print('Export CAD')
        dxf_filepath = drawing_camera.drawing_filepath.with_suffix('.dxf')

        dxf = generate_dxf(dxf_filepath)
        if drawing_camera.export_dwg:
            addon_preferences = get_addon_preferences()
            oda_file_converter = addon_preferences.oda_fc_path \
                    if addon_preferences else ''
            oda_cmd = f'{oda_file_converter} "{dxf_filepath.parent}"'
            oda_cmd += f' "{dxf_filepath.parent}"'
            oda_cmd += f' "ACAD2013" "DWG" "1" "1"'
            oda_cmd += f' "{dxf_filepath.name}"'
            subprocess.run(oda_cmd, shell=True)
        if not drawing_camera.export_dxf:
            remove_file(dxf_filepath)
    if len(os.listdir(drawing_camera.path)) == 0:
        os.rmdir(drawing_camera.path)
    for f in tmp_imgs + tmp_svgs:
        remove_file(f)
    progress.update(phase_name='main', step=next(main_progress_count))

    progress.end()

    if inspect.stack()[1].function == '<module>':
        reset_scene()
        print(f"\n--- Completed in {time.time() - start_time} seconds ---\n\n")

    return drawing_camera.drawing_filepath

if __name__ == "__main__":
    main()
