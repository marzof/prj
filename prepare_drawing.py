#!/usr/bin/env python3.9
# -*- coding: utf-8 -*- 

# Copyright (c) 2021 Marco Ferrara

# License:
# GNU GPL License
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.


import bpy
import sys
from prj.utils import is_renderable
from prj.drawing_camera import get_drawing_camera
from prj.working_scene import get_working_scene
from prj.subjects_getter import get_in_frame_subjects
from prj.progress_indicator import get_progress_indicator
from prj.visibility_checker import get_not_occluded_subjects
from prj.geometry_utils import is_cut
from prj.progress_indicator import get_progress_indicator

to_be_drawn = lambda subj: subj.xray_drawing or subj.force_drawing \
        or subj.is_symbol or subj.back_drawing

def process_args() -> tuple:
    """ Get objects and camera from sys.argv or from selection """
    cam, objs = None, []
    args = [arg for arg in sys.argv[sys.argv.index("--") + 1:]]
    print('args', args)
    args_objs_names = ''.join(args).split(';') if args else []
    for obj_name in args_objs_names:
        if bpy.data.objects[obj_name].type == 'CAMERA':
            cam = bpy.data.objects[obj_name]
            continue
        if is_renderable(bpy.data.objects[obj_name]):
            objs.append(bpy.data.objects[obj_name])
    if not cam:
        for obj in bpy.context.selected_objects:
            if obj.type != 'CAMERA':
                continue
            cam = obj
            break
    if not objs:
        for obj in bpy.context.selected_objects:
            if not is_renderable(obj):
                continue
            objs.append(obj)
    return cam, objs

def get_subjects() -> list['Drawing_subject']:
    """ Find all visible subjects (symbols, forced, cut and back subjects 
        are added anyway) and move them to the working scene """
    subjects = []
    working_scene = get_working_scene()
    scene = working_scene.scene
    drawing_camera = get_drawing_camera() 
    ## Get all subjects inside camera frame
    
    in_frame_subjects = get_in_frame_subjects()
    #next(main_progress_count) ## -> get_in_frame_subjects @ subject_getter

    if drawing_camera.disable_render:
        not_occluded_subjects = in_frame_subjects
    else:
        not_occluded_subjects = get_not_occluded_subjects(in_frame_subjects, 
                drawing_camera.data, working_scene)

    progress = get_progress_indicator()
    progress.set_phase(phase_data={
        'name': 'get_subjects', 'start':progress.get_last_update(), 
        'end':progress.get_next_step('main'), 'steps':len(in_frame_subjects)})

    ## Remove not visible subjects and add others to working_scene
    for i, subj in enumerate(in_frame_subjects):
        progress.update(phase_name='get_subjects', step=i)
        if to_be_drawn(subj):
            print(subj.name, 'is TO DRAW')
            working_scene.add_subject(subj)
            subjects.append(subj)
            continue
        if subj.is_cut and is_cut(subj.mesh_eval, subj.matrix, 
                drawing_camera.frame, drawing_camera.direction, scene):
            print(subj.name, 'is CUT')
            working_scene.add_subject(subj)
            subjects.append(subj)
            continue
        if subj not in not_occluded_subjects:
            print(subj.name, 'is NOT VISIBLE')
            subj.remove()
            continue
        print(subj.name, 'is VISIBLE')
        working_scene.add_subject(subj)
        subjects.append(subj)
    return subjects
