#!/usr/bin/env python3.9
# -*- coding: utf-8 -*- 

import xml.etree.ElementTree as ET
import re
import prj.svglib as svglib
from prj.utils import debug_here

NS_RE = re.compile(r'{(.*)}(.*)')

tags = {'svg': {'class': 'AbsSvg_drawing', 'get_data': lambda **data: []},
        'defs': {'class': 'AbsDefs', 'get_data': lambda **data: []},
        'g': {'class': 'AbsGroup', 'get_data': lambda **data: []},
        'style': {'class': 'AbsStyle', 
            'get_data': lambda **data: [i.text for i in data['element'].iter()]},
        'polyline': {'class': 'AbsPolyline', 
            'get_data': lambda **data: [get_polyline_points(data['element'])]},
        'use': {'class': 'AbsUse', 
            'get_data': lambda **data: [data['attribs']['xlink:href']]},
        }

xml_match = '<?xml'
xml_tag = '<?xml version="1.0" encoding="UTF-8"?>'

def get_polyline_points(element:ET.Element) -> list[tuple[float]]:
    """ Return the points coords of element """
    points = []
    raw_points: list[str] = element.attrib['points'].split()
    for coords in raw_points:
        xy = []
        for co in coords.split(','):
            xy.append(float(co))
        points.append(tuple(xy))
    return points

def clean_svg_file(filepath) -> None:
    """ Fix non-standard xml file beginning """
    xml_start = 0
    with open(filepath, "r") as f:
        lines = f.readlines()
    for i, line in enumerate(lines):
        if xml_match in line:
            xml_start = i
            break
    with open(filepath, "w") as f:
        f.write(xml_tag)
        for line in lines[xml_start+1:]:
            f.write(line)


# # # # CLASSES # # # #

class Svg_read:
    root: ET.Element
    namespaces: dict[str, str]
    tree: dict[tuple[int], ET.Element]

    def __init__(self, filepath: str):
        clean_svg_file(filepath)
        self.filepath = filepath
        self.root = ET.parse(filepath).getroot()
        self.namespaces = dict([node for _, node in 
            ET.iterparse(filepath, events=['start-ns'])])
        self.ns_keys = [*self.namespaces.keys()]
        self.ns_values = [*self.namespaces.values()]
        self.tree = {}
        self._get_tree(self.root)
        self.drawing = self.tree[(0,)]
        self.data = {}

    def get_entities_by_tag(self, tag: str) -> list['AbsSvg_entity']:
        entities = []
        for entity in self.tree.values():
            if entity.tag == tag:
                entities.append(entity)
        return entities

    def _get_tree(self, element: ET.Element, position: tuple[int] = (0,)) -> None:
        """ Populate element tree with abstract version of entities """
        abs_svg = self.get_abs_svg_class(element)
        if not abs_svg:
            return
        self.tree.update({position: abs_svg})
        if len(position) > 1:
            parent = self.tree[position[:-1]]
            parent.add_entity(abs_svg)
        for i, child in enumerate(element):
            self._get_tree(child, position + (i,))

    def get_abs_svg_class(self, element: ET.Element) -> 'svglib.AbsSvg_entity':
        """ Return an abstract version of element """
        svg_ns = f'{{{self.namespaces[""]}}}'
        tag = element.tag[len(svg_ns):]
        attribs = {self.ns_to_attribs(k): v for k,v in element.attrib.items()}
        ## TODO handle title and desc as well
        if tag == 'title' or tag == 'desc':
            return None
        abs_class = tags[tag]['class']
        data = tags[tag]['get_data'](element = element, attribs = attribs)
        if tag == 'g' and 'inkscape:groupmode' in attribs:
            abs_class = 'AbsLayer'
            data = [attribs['inkscape:label']]
        svg_class_constructor = getattr(svglib, abs_class)
        abs_svg = svg_class_constructor(*data)
        abs_svg.set_attribute(attribs)
        return abs_svg

    def ns_to_attribs(self, attrib_key: str) -> dict[str, str]:
        """ Convert element attrib_key to corresponding namespace """
        new_attrib_key = attrib_key
        ns_in_attrib = NS_RE.match(attrib_key)
        if ns_in_attrib:
            idx = self.ns_values.index(ns_in_attrib.group(1))
            new_attrib_key = f'{self.ns_keys[idx]}:{ns_in_attrib.group(2)}'
        return new_attrib_key

    def set_data(self, key: str, data):
        self.data[key] = data

    def get_data(self, key: str):
        return self.data[key]


