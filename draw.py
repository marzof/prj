#!/usr/bin/env python3.9
# -*- coding: utf-8 -*- 

# Copyright (c) 2021 Marco Ferrara

# License:
# GNU GPL License
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import time
from prj.utils import add_modifier, debug_here
from prj.drawing_subject import get_subjects_list, reset_subjects_list
from prj.working_scene import get_working_scene
from prj.progress_indicator import get_progress_indicator
from prj.drawing_camera import get_drawing_camera
from prj.drawing_maker import draw

def set_subjects_visibility(subject: 'Drawing_subject') -> None:
    """ Hide and show subjects depending on situation """
    subject.obj.hide_viewport = False
    if subject.has_back_stairs():
        subject.back_stairs_obj.hide_viewport = False
    drawing_subjects = get_subjects_list()
    for other_subj in drawing_subjects:
        if other_subj == subject:
            continue
        if other_subj.has_back_stairs():
            other_subj.back_stairs_obj.hide_viewport = True
            continue
        if other_subj.xray_drawing:
            other_subj.obj.hide_viewport = True
            continue
        if other_subj not in subject.overlapping_subjects:
            other_subj.obj.hide_viewport = True
            continue
        other_subj.obj.hide_viewport = False

def prepare_subjects(subjects: list['Drawing_subject']) -> None:
    """ Prepare cut objects, simplify meshes and apply stairs cuts """

    print('Prepare drawings')
    prepare_start_time = time.time()
    working_scene = get_working_scene()
    drawing_camera = get_drawing_camera()

    ## Hide frame box (which cuts out cut objects to frame boundary)
    drawing_camera.frame_box.hide_set(True)

    to_cut = []
    to_simplify = []
    to_stairs_cut = []
    for i, subject in enumerate(subjects):
        if subject.is_symbol:
            if not subject.is_stairs_cut:
                continue
            to_stairs_cut.append(subject)
        if subject.is_cut:
            to_cut.append(subject)
        if subject.hidden_drawing:
            continue
        to_simplify.append(subject)

    progress = get_progress_indicator()
    progress.set_phase(phase_data={
        'name': 'prepare_subjects', 'start':progress.get_last_update(), 
        'end':progress.get_next_step('main'), 
        'steps':len(to_cut + to_simplify + to_stairs_cut)})

    j = 0
    for i, subject in enumerate(to_stairs_cut): 
        progress.update(phase_name='prepare_subjects', step=i)
        print('Apply stairs cut for', subject.name)
        subject.apply_stairs_cut()
        j += 1

    for i, subject in enumerate(to_cut, start=j):
        progress.update(phase_name='prepare_subjects', step=i)
        if subject.stairs_cut_ref:
            continue
        print('Create cut for', subject.name)
        cut_obj = subject.create_cut()
        if not subject.is_open_cut:
            ## Don't apply frame box cut on open cut lines 
            add_modifier(cut_obj, 'prj_frame_cut_bool', 'BOOLEAN',
                    {'object': drawing_camera.frame_box, 
                        'operation': 'INTERSECT', 'solver': 'FAST'})
        if cut_obj.name not in working_scene.scene.collection.objects:
            working_scene.link_object(cut_obj)
        if subject.cut_only_drawing:
            working_scene.unlink_object(subject.obj)

        j += 1
        #if subject.name == 'wall_037':
        #    debug_here()

    ## TODO Remove faces behind camera too (if not back drawing)
    for i, subject in enumerate(to_simplify, start=j):
        progress.update(phase_name='prepare_subjects', step=i)
        print('Simplify mesh for', subject.name)
        subject.get_visible_mesh()

    preparing_time = time.time() - prepare_start_time
    print(f"Preparing subjects in {preparing_time} seconds")

def draw_subjects(subjects: list['Drawing_subject']) -> None:
    """ Get exported svgs for every subject (or parts of it) for every style """
    drawing_times: dict[float, str] = {}
    draw_time = time.time()
    ## Resolution percentage affects grease pencil svg export: reset it to 100
    get_working_scene().set_resolution_percentage(100)

    progress = get_progress_indicator()
    progress.set_phase(phase_data={'name': 'draw_subjects',
                'start':progress.get_last_update(), 
                'end':progress.get_next_step('main'), 'steps':len(subjects) })

    ## Draw every subject (and hide not overlapping ones)
    for i, subject in enumerate(subjects):
        print('Drawing', subject.name)
        progress.update(phase_name='draw_subjects', step=i)

        drawing_start_time = time.time()
        set_subjects_visibility(subject)
        draw(subject)

        ## It misses same-time drawing objects
        drawing_time = time.time() - drawing_start_time
        drawing_times[drawing_time] = subject.name
        print(f"\t...drawn in {drawing_time} seconds")

    draw_time = time.time() - draw_time
    print('\n')
    for t in sorted(drawing_times):
        print(drawing_times[t], t)
    print(f"***Drawings completed in {draw_time} seconds")
    return 
