#!/usr/bin/env python3.9
# -*- coding: utf-8 -*- 

import os
import bpy, bmesh
from mathutils import Vector
from prj.utils import flatten, debug_here
from functools import reduce

def join_meshes(obj_name: str, objects: list[bpy.types.Object]) \
        -> bpy.types.Object:
    """ Join objects in a new mesh object and add it to scene """
    depsgraph = bpy.context.evaluated_depsgraph_get()
    mesh = bpy.data.meshes.new(obj_name)
    bm = bmesh.new()
    verts_len = [0]
    for obj in objects:
        ## Join obj data to bmesh
        bm.from_mesh(obj.data)
        verts_len.append(len(bm.verts))
        ## Fix position of last added obj verts
        for vert in bm.verts[verts_len[-2]:verts_len[-1]]:
            vert.co = obj.matrix_world @ vert.co
        ## Make mesh non-manifold
        bmesh.ops.remove_doubles(bm, verts=bm.verts[:], dist=.00001)
    ## Fix normals orientation
    bmesh.ops.recalc_face_normals(bm, faces=bm.faces)
    for face in bm.faces:
        face.normal_update()
    bm.to_mesh(mesh)
    bm.free()
    new_obj = bpy.data.objects.new(name=obj_name, object_data=mesh)
    return new_obj

def is_open() -> bool:
    """ Check if mesh line (is supposed to be a wire, no faces) is open 
        (some vert is connected to just one edge) """
    mesh = bpy.context.object.data 
    bm = bmesh.from_edit_mesh(mesh) 
    bmesh.ops.remove_doubles(bm, verts=bm.verts[:], dist=.00001)
    is_open_mesh = False
    for v in bm.verts:
        if len(v.link_edges) > 1:
            continue
        is_open_mesh = True
        break
    return is_open_mesh

def get_bmesh_fill_area(obj):
    bm = bmesh.new()
    bm.from_mesh(obj.data)
    bmesh.ops.triangle_fill(bm, use_beauty=True, edges=bm.edges[:])
    return bm

def cut_by_plane(obj: bpy.types.Object, plane_co: 'mathutils.Vector', 
        plane_no: 'mathutils.Vector', keep_on_scene: bool = True,
        suffix: str = '_cut') -> bpy.types.Object:
    """ Cut obj by plane and return a new object """
    obj_dupli = obj.copy()
    obj_dupli.data = obj.data.copy()
    bpy.context.scene.collection.objects.link(obj_dupli)
    obj_dupli.hide_viewport = False

    obj_dupli.select_set(True)
    bpy.context.view_layer.objects.active = obj_dupli
    bpy.ops.object.mode_set(mode='EDIT')
    bpy.ops.mesh.select_all(action = 'SELECT')
    bpy.ops.mesh.bisect(plane_co=plane_co, plane_no=plane_no, use_fill=False, 
            clear_inner=True, clear_outer=True, threshold=0.0001)
    if not is_open():
        ## Fill cut mesh
        bpy.ops.mesh.edge_face_add()
        ## Try another filling metod
        bm = get_bmesh_fill_area(obj)

        mesh_fill_area = reduce((lambda x, y: x + y), [face.area for face 
            in obj_dupli.data.polygons])
        bmesh_fill_area = reduce((lambda x, y: x + y), [face.calc_area()
            for face in bm.faces])
        ## Compare filling results: the smallest is the right one
        if bmesh_fill_area < mesh_fill_area:
            bm.to_mesh(obj_dupli.data)
        else:
            bpy.ops.mesh.quads_convert_to_tris(quad_method='BEAUTY', 
                    ngon_method='BEAUTY')
        bm.free()

    bpy.ops.object.mode_set(mode='OBJECT')
    obj_dupli.select_set(False)

    if not keep_on_scene:
        bpy.context.scene.collection.objects.unlink(obj_dupli)
    return obj_dupli

def get_verts_order(bm: bmesh.types.BMesh, 
        verts_lists: list[list[bmesh.types.BMVertSeq]]) \
                -> list[list[bmesh.types.BMVertSeq]]:
    """ Order the vertices of bm and return a list of ordered vertices 
        sequences """
    result = None
    for v in bm.verts:
        if v in flatten(verts_lists):
            continue
        if not result:
            verts_lists.append([])
        verts_lists[-1].append(v)
        result = get_next_vert(v, verts_lists)
    return verts_lists

def get_next_vert(v: bmesh.types.BMVertSeq, 
    verts_lists: list[list[bmesh.types.BMVertSeq]]) -> None:
    """ Starting by v, add vertices to verts_lists[-1] in connection order 
        (every vert is connected by edge with the next) """
    for e in v.link_edges:
        other_vert = e.other_vert(v)
        if other_vert in flatten(verts_lists):
            continue
        verts_lists[-1].append(other_vert)
        get_next_vert(other_vert, verts_lists)

def remove_doubles(mesh_obj: bpy.types.ID) -> bpy.types.ID:
    """ Remove doubles from mesh_obj (it could be as well an Object or a Mesh"""
    if type(mesh_obj) == bpy.types.Object:
        mesh = mesh_obj.data
    else:
        mesh = mesh_obj

    bm = bmesh.new()
    bm.from_mesh(mesh)
    bmesh.ops.remove_doubles(bm, verts=bm.verts[:], dist=.00001)

    if type(mesh_obj) == bpy.types.Object:
        bm.to_mesh(mesh_obj.data)
    else:
        bm.to_mesh(mesh_obj)

    bm.free()
    return mesh_obj

def remove_bad_oriented_faces(obj: bpy.types.Object, 
        direction: 'mathutils.Vector') -> bpy.types.Object:
    """ Remove from obj all faces pointed to the opposite side of direction 
        (and clean every edges or verts not connected to a face) """
    me = obj.data
    bm = bmesh.new()
    bm.from_mesh(me)
    bmesh.ops.remove_doubles(bm, verts=bm.verts[:], dist=.00001)
    for face in bm.faces:
        face_normal_world = obj.matrix_world.to_3x3().normalized() @ face.normal
        dot_product = direction @ face_normal_world
        if round(dot_product,5) >= 0:
            bm.faces.remove(face)  
    for edge in bm.edges:
        if edge.is_wire:
            bm.edges.remove(edge)
    for vert in bm.verts:
        if len(vert.link_faces) == 0:
            bm.verts.remove(vert)
    bm.to_mesh(me)
    bm.free()
    return obj

def create_line_obj(name: str, vertices: list['mathutils.Vector']) \
        -> bpy.types.Object:
    """ Return a new mesh object using vertices """
    mesh = bpy.data.meshes.new(name)
    bm = bmesh.new()
    verts = [bm.verts.new(v) for v in vertices]
    edges = [bm.edges.new((verts[i], verts[i+1])) for i in range(len(verts)-1)]
    bm.to_mesh(mesh)
    bm.free()
    obj = bpy.data.objects.new(name, mesh)
    return obj

def create_face_obj(name: str, verts: tuple[Vector], 
        verts_offset: Vector = Vector((0,0,0))) -> bpy.types.Object:
    """ Create a single face object from verts. 
        An optional verts_offset can be applied """
    face_mesh = bpy.data.meshes.new(name)
    face_obj = bpy.data.objects.new(name, face_mesh) 
    bm = bmesh.new()
    for v in verts:
        bm.verts.new(v + verts_offset)
    bm.faces.new(bm.verts)
    bm.to_mesh(face_mesh)  
    bm.free() 
    return face_obj
