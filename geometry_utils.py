#!/usr/bin/env python3.9
# -*- coding: utf-8 -*- 

import bpy
import math
from mathutils import Vector, geometry
from prj.utils import debug_here

def is_parallel_to_plane(obj: bpy.types.Object, obj_matrix: 'mathutils.Matrix', 
        normal: Vector) -> bool:
    """ Check if obj z axis (according to matrix) is parallel to normal """
    tolerance: int = 5
    obj_Z_axis = Vector((obj_matrix[0][2],obj_matrix[1][2],obj_matrix[2][2]))
    angle = obj_Z_axis.angle(normal)
    if round(angle - math.pi, tolerance) == 0:
        return True
    return False

def get_bounding_rect(vectors: list[Vector]) -> list[Vector]:
    """ Get the bounding rect of vectors """
    vectors_xs = [v.x for v in vectors]
    vectors_ys = [v.y for v in vectors]
    x_min = min(vectors_xs)
    x_max = max(vectors_xs)
    y_min = min(vectors_ys)
    y_max = max(vectors_ys)
    return [Vector((x_min, y_min)), Vector((x_max, y_min)), 
            Vector((x_max, y_max)), Vector((x_min, y_max))]

def get_framed_rect(rect: list[Vector], frame: list[Vector]) -> list[Vector]:
    """ Get the intersection area between rect and frame """
    rect_xs = [v.x for v in rect]
    rect_ys = [v.y for v in rect]
    frame_xs = [v.x for v in frame]
    frame_ys = [v.y for v in frame]
    x_min = max(min(frame_xs), min(rect_xs))
    x_max = min(max(frame_xs), max(rect_xs))
    y_min = max(min(frame_ys), min(rect_ys))
    y_max = min(max(frame_ys), max(rect_ys))
    if x_min > max(frame_xs) or x_max < min(frame_xs) \
            or y_min > max(frame_ys) or y_max < min(frame_ys):
        ## rect is out of frame
        return None
    return [Vector((x_min, y_min)), Vector((x_max, y_min)), 
            Vector((x_max, y_max)), Vector((x_min, y_max))]

def framed_box(box: list[Vector], frame: list[Vector]) -> dict[str,bool]:
    """ Check if box is inside (surrounded, intersecting or surrounding),
        in front of or behind frame """
    in_front = False
    behind = False

    ## Check if box is in_front of and/or behind frame
    zs = [v.z for v in box]
    z_min, z_max = min(zs), max(zs)
    if frame[0].z <= z_max: 
        in_front = True
    if z_min <= frame[0].z: 
        behind = True

    ## Check if a box vertex in inside frame
    xs, ys = [v.x for v in frame], [v.y for v in frame]
    for v in box:
        if min(xs) <= v.x <= max(xs) and min(ys) <= v.y <= max(ys):
            return {'result': True, 'in_front': in_front, 'behind': behind}

    ## Check if frame is totally inside box 
    vxs, vys = [v.x for v in box], [v.y for v in box]
    box_min = {'x': min(vxs), 'y': min(vys)}
    box_max = {'x': max(vxs), 'y': max(vys)}
    for v in frame:
        frame_x_is_in = box_min['x'] <= v.x <= box_max['x']
        frame_y_is_in = box_min['y'] <= v.y <= box_max['y']
        if not frame_x_is_in or not frame_y_is_in:
            continue
        return {'result': True, 'in_front': in_front, 'behind': behind}

    ## Check if box intersect frame
    frame_sides = []
    checked_vectors = []
    for fv in frame:
        if fv in checked_vectors:
            continue
        checked_vectors.append(fv)
        for v in frame:
            if fv == v:
                continue
            if v.x != fv.x and v.y != fv.y:
                continue
            checked_vectors.append(v)
            frame_sides.append((fv.resized(2), v.resized(2)))
    bound_box_edge_idxs = [(0,1),(0,3),(0,4), (1,2), (1,5), (2,3), (2,6),
            (3,7), (4,5), (4,7), (5,6), (6,7)]
    checked_edges = []
    for edge in bound_box_edge_idxs:
        edge = (box[edge[0]].resized(2), box[edge[1]].resized(2))
        if (edge[0] - edge[1]) == 0:
            continue
        if edge in checked_edges:
            continue
        if (edge[1], edge[0]) in checked_edges:
            continue
        checked_edges.append(edge)

        for side in frame_sides:
            intersection = geometry.intersect_line_line_2d(edge[0], edge[1], 
                    side[0], side[1])
            if intersection:
                return {'result': True, 'in_front': in_front, 'behind': behind}
    return {'result': False, 'in_front': in_front, 'behind': behind}

def vector_round(vector: Vector, rounding: int):
    x = round(vector.x, 4)
    y = round(vector.y, 4)
    z = round(vector.z, 4)
    return Vector((x,y,z))

def is_cut(mesh: bpy.types.Mesh, matrix: 'mathutils.Matrix', 
        quad_verts: list[Vector], quad_normal: Vector, scene: bpy.types.Scene) \
                -> bool:
    """ Check if an edge of mesh intersect quad_verts quad plane (at least an 
        edge intersect needs to intersect the quad_verts quad plane) """
    for edge in mesh.edges:
        verts = edge.vertices
        v0 = matrix @ mesh.vertices[verts[0]].co
        v1 = matrix @ mesh.vertices[verts[1]].co
        ## line and plane are extended to find intersection
        intersection_raw =  geometry.intersect_line_plane(
                v0, v1, quad_verts[0], quad_normal)
        if not intersection_raw: ## no intersection: edge and quad are parallel
            continue
        intersection = vector_round(intersection_raw, 4)
        point_on_line = geometry.intersect_point_line(intersection, v0, v1)
        distance_from_line = point_on_line[1]
        if not 0 <= distance_from_line <= 1: ## intersection is on edge extension
            continue
        point_on_tri_0 = geometry.closest_point_on_tri(intersection, 
                quad_verts[0], quad_verts[1], quad_verts[2])
        point_on_tri_1 = geometry.closest_point_on_tri(intersection, 
                quad_verts[2], quad_verts[3], quad_verts[0])
        distance0 = intersection - point_on_tri_0
        distance1 = intersection - point_on_tri_1
        distance = round(min(distance0.length, distance1.length), 4)
        if distance != 0: ## intersection is not inside quad_verts frame
            continue
        return True
    return False

