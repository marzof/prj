#!/usr/bin/env python3.9
# -*- coding: utf-8 -*- 

# Copyright (c) 2021 Marco Ferrara

# License:
# GNU GPL License
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies: 
# TODO...


import bpy
import math
import os
import tempfile
from pathlib import Path as Filepath
from prj.utils import make_active, add_modifier, create_grease_pencil, debug_here
from prj.drawing_camera import get_drawing_camera
from prj.drawing_subject import get_subjects_list
from prj.working_scene import get_working_scene
from prj.svgread import Svg_read

cam_z_offset = .01
tmp_svgs = []

def get_lineart(source: 'Drawing_subject', style: 'Drawing_style') \
        -> bpy.types.Object:
    """ Create source.grease_pencil if needed and add a lineart modifier 
        with style to it """
    working_scene = get_working_scene()
    if not source.has_grease_pencil:
        new_gp = create_grease_pencil('gp_' + source.obj.name) 
        working_scene.scene.collection.objects.link(new_gp)
        source.set_grease_pencil(new_gp) 
    occlusion_end = 0 if source.has_back_stairs() else style.occlusion_end
    use_custom_camera = style.style == 'c'
    gp = source.get_grease_pencil()
    gp_modifier = add_modifier(obj = gp, 
            mod_name = 'lineart_' + style.name, mod_type = 'GP_LINEART', 
            params = {
                'target_layer': gp.data.layers[0].info,
                'target_material': gp.data.materials[0],
                'chaining_image_threshold': 0.001 if style.name == "prj" else 0,
                'crease_threshold': math.radians(140),
                'use_multiple_levels': True,
                #'use_remove_doubles': True,
                'use_edge_overlap': True,
                'use_edge_mark': True,
                'use_crease': not source.draw_outline,
                'use_crease_on_sharp': False,
                'use_intersection': style.name == "prj",
                'use_fuzzy_intersections': style.name == "prj",
                'use_clip_plane_boundaries': False,
                #'use_ortho_tolerance': True,
                'use_custom_camera': use_custom_camera,
                'source_camera': get_drawing_camera().larger_one,
                'use_image_boundary_trimming': True,
                'level_start': style.occlusion_start,
                'level_end': occlusion_end,
                'smooth_tolerance': 0.0,
                'split_angle': 0.0,
                'source_type': 'OBJECT',
                'source_object': source.get_obj_to_draw(style)
                }, 
            gpencil = True)

    source.add_gp_modifier(gp_modifier)
    return gp

def export_grease_pencil(subject: 'Drawing_subject', 
        grease_pencil: bpy.types.Object, svg_suffix: str = '',
        scene: bpy.types.Scene = None) -> None:
    """ Export grease_pencil to svg and add its content to subject.svg_data """
    global tmp_svgs
    if not scene:
        scene = bpy.context.scene

    fd, tmp_file = tempfile.mkstemp(suffix='.svg')
    make_active(grease_pencil, scene)
    with open(tmp_file, 'wb') as f:
        bpy.ops.wm.gpencil_export_svg(filepath=tmp_file, 
                selected_object_type='VISIBLE') 
    ## TODO use_clip_camera=True causes error -> need to do actual clipping

    ## Get data from newly created svg and add to subject.svg_data
    svg_data = Svg_read(tmp_file)
    svg_data.set_data('subject', subject)
    svg_data.set_data('style', svg_suffix)
    subject.svg_data.add_data(svg_data)
    tmp_svgs.append(tmp_file)
    os.close(fd)

def draw(subject: 'Drawing_subject', remove: bool = True) -> list[str]:
    """ Create a grease pencil for subject (and add a lineart modifier) for
        every draw_style. Then export the grease pencil """
    drawing_camera = get_drawing_camera()
    scene = get_working_scene().scene
    drawing_subjects = get_subjects_list()
    for style in subject.styles:
        if subject.has_back_stairs():
            ## Move camera above stairs to get the back view
            offset = -(subject.top_vert_dist - drawing_camera.clip_start - 1)
            drawing_camera.move_along_z(offset)
            if style.style == 'b':
                subject.back_stairs_obj.hide_viewport = False
            else:
                subject.back_stairs_obj.hide_viewport = True
        elif style.style == 'b':
            drawing_camera.reverse()
        elif style.style == 'x':
            ## Xray subjects don't have to be occluded
            for subj in drawing_subjects:
                if subj.cut_obj:
                    subj.cut_obj.hide_viewport = True
                if subj == subject:
                    continue
                subj.obj.hide_viewport = True

        print(f'draw {subject.original_name} in style {style.name}')
        ## Move camera away from cutting plane to avoid drawing artefacts
        drawing_camera.move_along_z(cam_z_offset)
        lineart_gp = get_lineart(source=subject, style=style)
        ## In order to update lineart visibility set a frame (twice)
        bpy.context.scene.frame_set(1)
        bpy.context.scene.frame_set(1)
        #if subject.original_name == 'Tech_lid':
        #if style.style == 'c':
        #    debug_here()
        export_grease_pencil(subject=subject,grease_pencil=lineart_gp,
                svg_suffix=style.name, scene=scene)
        #debug_here()
        ## Reset camera position
        drawing_camera.move_along_z(-cam_z_offset)
        if style.style == 'b' or subject.has_back_stairs():
            drawing_camera.reset()
        elif style.style == 'x':
            ## Reset cut objects visibility
            for subj in drawing_subjects:
                if subj.cut_obj:
                    subj.cut_obj.hide_viewport = False
        subject.remove_gp_modifier()
    subject.remove_grease_pencil()
