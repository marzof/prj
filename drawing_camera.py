#!/usr/bin/env python3.9
# -*- coding: utf-8 -*- 

# Copyright (c) 2021 Marco Ferrara

# License:
# GNU GPL License
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies: 
# TODO...


import bpy
import os
from mathutils import Vector, Matrix
from pathlib import Path as Filepath
from prj.working_scene import get_working_scene
from prj.utils import name_cleaner, cam_frame_factors, get_render_basepath
from prj.utils import add_modifier, debug_here
from prj.mesh_utils import create_face_obj

the_drawing_camera = None

def get_drawing_camera(camera: bpy.types.Object = None) -> 'Drawing_camera':
    """ Return the_drawing_camera (create it if necessary) """
    global the_drawing_camera
    if the_drawing_camera:
        return the_drawing_camera
    if not camera and not the_drawing_camera:
        return
    the_drawing_camera = Drawing_camera(camera)
    return the_drawing_camera

class Drawing_camera:
    obj: bpy.types.Object
    data: bpy.types.Camera
    name: str
    working_scene = 'Working_scene'
    path: str
    direction: Vector
    ortho_scale: float
    clip_start: float
    clip_end: float
    matrix: 'mathutils.Matrix'
    symbol_range: float
    local_frame: list[Vector]
    frame: list[Vector]
    lod: int
    larger_one: bpy.types.Object ## camera
    frame_box: bpy.types.Object

    def __init__(self, camera: bpy.types.Object):
        duplicate = camera.copy()
        duplicate.data = camera.data.copy()
        self.obj = duplicate
        self.data = duplicate.data
        self.linking_type = camera.data.prj_drawing_settings.linking_type
        self.disable_render = camera.data.prj_drawing_settings.disable_render
        self.export_svg = camera.data.prj_drawing_settings.export_svg
        self.export_dxf = camera.data.prj_drawing_settings.export_dxf
        self.export_dwg = camera.data.prj_drawing_settings.export_dwg
        self.export_cad = self.export_dxf or self.export_dwg
        drawing_name = camera.data.prj_drawing_settings.drawing_name
        draw_cam_name = drawing_name if drawing_name else camera.name
        self.original_name = camera.name
        self.name = name_cleaner(draw_cam_name).lower()
        self.ortho_scale = camera.data.ortho_scale
        ## Update drawing_camera location with shiftings 
        ## and remove their value from drawing_camera.data
        translation = Vector((camera.data.shift_x * self.ortho_scale,
            camera.data.shift_y * self.ortho_scale, 0))
        self.obj.matrix_world = Matrix.Translation(translation) @ \
                self.obj.matrix_world
        self.data.shift_x, self.data.shift_y = 0, 0

        self.working_scene = get_working_scene()
        self.working_scene.link_object(self.obj)
        self.working_scene.scene.camera = self.obj
        self.path = self.get_path()
        self.svg_path = Filepath(self.path + '.svg')
        self.drawing_filepath = self.get_drawing_filepath()
        self.direction = camera.matrix_world.to_quaternion() @ \
                Vector((0.0, 0.0, -1.0))
        self.clip_start = camera.data.clip_start
        self.clip_end = camera.data.clip_end
        self.matrix = self.obj.matrix_world.copy()
        self.matrix_local = self.obj.matrix_local.copy()
        self.symbol_range = (camera.data.clip_start, camera.data.clip_start \
                + camera.data.prj_drawing_settings.view_symbol)
        self.frame_factors = cam_frame_factors(self.data)
        self.local_frame = [v * Vector((1,1,self.clip_start)) 
                for v in self.data.view_frame(scene=self.working_scene.scene)]
        self.normalized_frame = [
                    Vector((1,1,self.clip_start)),
                    Vector((1,0,self.clip_start)),
                    Vector((0,0,self.clip_start)),
                    Vector((0,1,self.clip_start))]
        self.frame = [self.matrix @ v for v in self.local_frame]
        self.lod = camera.data.prj_drawing_settings.level_of_detail

        ## Create a camera duplicate with larger frame 
        ## in order to get border linearts
        self.larger_one = camera.copy()
        self.larger_one.data = camera.data.copy()
        self.larger_one.data.driver_remove("ortho_scale")
        self.larger_one.data.ortho_scale = camera.data.ortho_scale * 1.01
        ## Parenting binds larger_one to drawing_camera movements
        self.larger_one.parent = self.obj 
        self.larger_one.matrix_world = self.matrix
        self.working_scene.link_object(self.larger_one)

        ## Create a box as frame extrusion 
        ## in order to limit cut objects to drawing
        cam_frame = self.data.view_frame()
        frame_factors_v = Vector(list(self.frame_factors.values()) + [1])
        frame_verts = [frame_vert * frame_factors_v for frame_vert in cam_frame]
        self.frame_box = create_face_obj(name = 'frame_box',  verts= frame_verts,
                verts_offset = Vector((0,0,1)))
        self.working_scene.link_object(self.frame_box)
        self.frame_box.matrix_world = self.matrix
        add_modifier(self.frame_box, 'Solidify_extrude', 'SOLIDIFY', 
                {'thickness': 2*self.clip_end, 'offset': 0})

    def get_drawing_filepath(self):
        base_path = Filepath(bpy.path.abspath('//'))
        return base_path / self.svg_path

    def get_path(self, create: bool = True) -> str:
        """ Return folder path named after camera (create it if needed) """
        ## TODO add subpath option like 
        ## cam_path = os.path.join(get_render_basepath(), self.subpath, self.name)
        cam_path = os.path.join(get_render_basepath(), self.name)
        if not create:
            return cam_path
        if self.linking_type == 'linked':
            try:
                os.mkdir(cam_path)
            except OSError:
                print (f'{cam_path} already exists. Going on')
        return cam_path

    def move_along_z(self, offset: float) -> None:
        """ Move camera along its view direction """
        translation = offset * \
                Vector((self.matrix[0][2], self.matrix[1][2], self.matrix[2][2]))
        ## If use self.obj.location+=translation 
        ## then need updating by bpy.context.view_layer.update()
        self.obj.matrix_world = Matrix.Translation(translation) @ \
                self.obj.matrix_world

    def reverse(self, offset: float = None) -> None:
        """ Invert camera direction keeping frame position.
            A different offset can be provided """
        ## Translate_camera_along_z
        if not offset:
            offset = -2 * self.clip_start
        self.move_along_z(offset)
        ## Reverse_camera
        self.obj.matrix_world @= Matrix().Scale(-1, 4, (.0,.0,1.0))

    def reset(self) -> None:
        """ Reset camera matrices to original status (but after shifting 
            adjustement) """
        self.obj.matrix_world = self.matrix
        self.obj.matrix_local = self.matrix_local

    def remove(self) -> None:
        bpy.data.cameras.remove(self.obj.data)
        bpy.data.cameras.remove(self.larger_one.data)
        global the_drawing_camera
        the_drawing_camera = None
