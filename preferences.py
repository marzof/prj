#   Dependencies installing modules are by Robert Guetzkow: 
#   https://github.com/robertguetzkow/blender-python-examples/

import bpy
import os
import sys
import subprocess
import importlib
from collections import namedtuple
from prj.utils import debug_here

# Declare all modules that this add-on depends on, that may need to be installed.
# The package and (global) name can be set to None, if they are equal to the 
# module name. See import_module and ensure_and_import_module for the explanation
# of the arguments. DO NOT use this to import other parts of your Python add-on,
# import them as usual with an "import" statement.
Dependency = namedtuple("Dependency", ["module", "package", "name"])
dependencies = (
        Dependency(module="svgwrite", package=None, name=None),
        Dependency(module="PIL", package="Pillow", name=None),
        Dependency(module="pyparsing", package=None, name=None), # for CAD export
        Dependency(module="ezdxf", package=None, name=None))     # for CAD export
dependencies_installed = None

def get_deps_installation() -> bool:
    if dependencies_installed == None:
        import_module(module_name=dependency.module, 
                global_name=dependency.name)
    return dependencies_installed

def set_deps_installation(val: bool) -> None:
    global dependencies_installed
    dependencies_installed = val

def import_module(module_name, global_name=None, reload=True):
    """
    Import a module.
    :param module_name: Module to import.
    :param global_name: (Optional) Name under which the module is imported. 
        If None the module_name will be used.
        This allows to import under a different name with the same effect 
        as e.g. "import numpy as np" where "np" is
        the global_name under which the module can be accessed.
    :raises: ImportError and ModuleNotFoundError
    """
    if global_name is None:
        global_name = module_name

    if global_name in globals():
        importlib.reload(globals()[global_name])
    else:
        # Attempt to import the module and assign it to globals dictionary. 
        # This allow to access the module under the given name, 
        # just like the regular import would.
        globals()[global_name] = importlib.import_module(module_name)

def install_pip():
    """
    Installs pip if not already present. Please note that ensurepip.bootstrap() 
    also calls pip, which adds the environment variable PIP_REQ_TRACKER. 
    After ensurepip.bootstrap() finishes execution, the directory doesn't exist
    anymore. However, when subprocess is used to call pip, in order to install 
    a package, the environment variables still contain PIP_REQ_TRACKER with the 
    now nonexistent path. This is a problem since pip checks if PIP_REQ_TRACKER
    is set and if it is, attempts to use it as temp directory. This would result 
    in an error because the directory can't be found. Therefore, 
    PIP_REQ_TRACKER needs to be removed from environment variables.
    :return:
    """

    try:
        # Check if pip is already installed
        subprocess.run([sys.executable, "-m", "pip", "--version"], check=True)
    except subprocess.CalledProcessError:
        import ensurepip

        ensurepip.bootstrap()
        os.environ.pop("PIP_REQ_TRACKER", None)

def install_and_import_module(module_name, package_name=None, global_name=None):
    """
    Installs the package through pip and attempts to import the installed module.
    :param module_name: Module to import.
    :param package_name: (Optional) Name of the package that needs to be 
        installed. If None it is assumed to be equal to the module_name.
    :param global_name: (Optional) Name under which the module is imported. 
        If None the module_name will be used.  
        This allows to import under a different name with the same effect as 
        e.g. "import numpy as np" where "np" is the global_name under which 
        the module can be accessed.
    :raises: subprocess.CalledProcessError and ImportError
    """
    if package_name is None:
        package_name = module_name

    if global_name is None:
        global_name = module_name

    # Blender disables the loading of user site-packages by default. However, 
    # pip will still check them to determine if a dependency is already 
    # installed. This can cause problems if the packages is installed in the user
    # site-packages and pip deems the requirement satisfied, but Blender cannot 
    # import the package from the user site-packages. Hence, the environment 
    # variable PYTHONNOUSERSITE is set to disallow pip from checking the user
    # site-packages. If the package is not already installed for Blender's 
    # Python interpreter, it will then try to.
    # The paths used by pip can be checked with 
    # `subprocess.run([bpy.app.binary_path_python, "-m", "site"], check=True)`

    # Create a copy of the environment variables and modify them for the 
    # subprocess call
    environ_copy = dict(os.environ)
    environ_copy["PYTHONNOUSERSITE"] = "1"


    subprocess.run([sys.executable, "-m", "pip", "install", package_name], 
            check=True, env=environ_copy)

    # The installation succeeded, attempt to import the module again
    import_module(module_name, global_name)

class PRJ_OT_install_dependencies(bpy.types.Operator):
    bl_idname = "prj.install_dependencies"
    bl_label = "Install dependencies"
    bl_options = {"REGISTER", "INTERNAL"}
    bl_description = (
            "Downloads and installs the required python packages for this "
            "add-on. Internet connection is required. Blender may have to be "
            "started with elevated permissions in order to install the package")

    @classmethod
    def poll(self, context):
        # Deactivate when dependencies have been installed
        return not get_deps_installation()

    def execute(self, context):
        try:
            install_pip()
            for dependency in dependencies:
                install_and_import_module(module_name=dependency.module,
                                          package_name=dependency.package,
                                          global_name=dependency.name)
        except (subprocess.CalledProcessError, ImportError) as err:
            self.report({"ERROR"}, str(err))
            return {"CANCELLED"}

        set_deps_installation(True)

        return {"FINISHED"}

def get_oda_fc_path(self):
    if 'oda_fc_path' not in self:
        return ''
    return self['oda_fc_path']

def set_oda_fc_path(self, value):
    """ Puth the path value in quotes to deal with spaces in path """
    self['oda_fc_path'] = f'"{os.path.abspath(value)}"'


class PRJ_preferences(bpy.types.AddonPreferences):
    bl_idname = __package__

    oda_fc_descr = "Path to ODA File Converter application: " + \
            "on Linux the usual location is /usr/bin/ODAFileConverter, " + \
            "on Windows it can be something like " + \
            "C:\Program Files\ODA\ODAFileConverter nn.n.n\ODAFileConverter.exe"

    oda_fc_path: bpy.props.StringProperty(name="prj_oda_fc_path", 
            description = oda_fc_descr, get=get_oda_fc_path, 
            set=set_oda_fc_path, subtype='FILE_PATH')

    def draw(self, context):
        layout = self.layout
        col = layout.column(align=True)
        col.label(text= 'PRJ need the following python libraries to work: ')
        for dep in dependencies:
            col.label(text = f'- {dep.module}')
        col.label(text= 'Click here to install them. ')
        col.label(text= '(on Windows computer you may need ' + \
                'to run Blender as administrator to install them)')
        col.operator(PRJ_OT_install_dependencies.bl_idname, icon="CONSOLE")
        col = layout.column(align=True)
        col.label(text= 'In order to export drawings in DWG format ' + \
                'you need to install ODA File Converter')
        col.label(text= 'and put its path in the field below')
        col.operator('wm.url_open', 
                text="Got to ODA File Converter site").url = \
                'https://www.opendesign.com/guestfiles/oda_file_converter'
        col.prop(self, "oda_fc_path", text="ODA File Converter path")

preference_classes = (PRJ_OT_install_dependencies, PRJ_preferences)

def register():
    set_deps_installation(False)

    for cls in preference_classes:
        bpy.utils.register_class(cls)

    try:
        for dependency in dependencies:
            import_module(module_name=dependency.module, 
                    global_name=dependency.name)
        set_deps_installation(True)
    except ModuleNotFoundError:
        # Don't register other panels, operators etc.
        return

def unregister():
    for cls in preference_classes:
        bpy.utils.unregister_class(cls)

if __name__ == "prj.preferences":
    register()
