#    Copyright (C) 2022  Marco Ferrara
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>

bl_info = {
    "name": "Prj",
    "author": "Marco Ferrara",
    "version": (0, 0, 9), #0.0.9m
    "blender": (3, 1, 2),
    "description": "Get technical drawing from model",
    "warning": "",
    ## "warning": "Requires installation of dependencies",
    "category": "3D View",
    ## TODO Add "doc_url" and "tracker_url"
}

import os
import site
cwd = os.path.dirname(os.path.realpath(__file__))
site.addsitedir(os.path.join(cwd, "libs", "site", "packages"))
import bpy
import re
import traceback
from prj.main import reset_scene, main
from prj.utils import is_renderable, debug_here, get_render_basepath, check_dir
from prj.progress_indicator import get_progress_indicator
import prj.preferences
import prj.drawing_limits
import prj.ui
import time

## TODO implement bulk copy (or link) of drawing options
## TODO add replaceable (in svg) symbols 
##      to make scale and orientation indipendent

filename_re = re.compile('[^\w\d_-]')

class Prj(bpy.types.Operator):
    """Set view to camera to export svg from grease pencil"""
    initial_space: bpy.types.Area
    initial_areas_view: dict[bpy.types.Area, str]
    initial_scene: bpy.types.Scene
    initial_scene_camera: bpy.types.Object
    area: bpy.types.Area
    selected_objects: list[bpy.types.Object]
    selected_cameras: list[bpy.types.Object]

    bl_idname = "prj.prj"
    bl_label = "Launch prj"
    bl_options = {'REGISTER', 'UNDO'}

    def __init__(self):
        self.selected_cameras = []
        self.selected_objects = []
        self.got_data = False

    def __del__(self):
        print('End')

    def reset_scene(self) -> None:
        """ Reset scene as it was before launching Prj """
        if self.selected_cameras:
            reset_scene()
        bpy.context.window.scene = self.initial_scene
        bpy.context.scene.camera = self.initial_scene_camera

        ## Reset view type for every 3d_viewport
        for area in self.initial_areas_view:
            area.type = 'VIEW_3D'
            view = self.initial_areas_view[area]
            area.spaces[0].region_3d.view_perspective = view
        ## Reset active area type
        self.area.type = self.initial_space.type

    @classmethod
    def poll(cls, context: bpy.types.Context) -> bool:
        return context.area.type in ['VIEW_3D', 'PROPERTIES', 'CONSOLE', 
                'TEXT_EDITOR']

    def execute(self, context):
        if not self.got_data:
            self.get_data(context)

        for camera in self.selected_cameras:
            print('Start now')
            start_time = time.time()

            try:
                for area in self.initial_areas_view:
                    if area == self.area:
                        continue
                    area.type = 'INFO'
                ## Change current area in a VIEW_3D to make svg export working
                self.area.type = 'VIEW_3D'
                r3d = self.area.spaces[0].region_3d
                r3d.view_perspective = "CAMERA"

                wm = context.window_manager
                get_progress_indicator(wm)
                filepath = main(camera, self.selected_objects, context)

                self.reset_scene()
                timing = time.time()-start_time
                timing_msg = f"\n--- Completed in {timing} seconds ---\n\n"
                print(timing_msg)
                info_msg = f"Drawing saved in {str(filepath)} "
                info_msg += f"(completed in {round(timing, 2)} seconds)"
                self.report({'INFO'}, info_msg)
                bpy.context.scene.unit_settings.length_unit = self.initial_units
            except Exception as exception:
                error_message = f'Something went wrong...Drawing cancelled '
                error_message += f'for {camera.name}\n{traceback.format_exc()}'
                self.report({'ERROR'}, error_message)
                self.reset_scene()

        return {'FINISHED'}


    def get_data(self, context: bpy.types.Context):
        self.initial_space = bpy.context.space_data
        self.initial_areas_view = {}
        for area in context.workspace.screens[0].areas:
            if area.type == 'VIEW_3D':
                ## Take note of view type for every 3d_viewport 
                ## (in order to reset them later)
                view_3d = area.spaces[0].region_3d.view_perspective
                self.initial_areas_view[area] = view_3d
            if area.spaces[0] == self.initial_space:
                ## Take note of active area editor
                self.area = area

        self.initial_scene = bpy.context.scene
        self.initial_units = bpy.context.scene.unit_settings.length_unit
        self.initial_scene_camera = bpy.context.scene.camera

        self.selected_objects = [obj for obj in bpy.context.selected_objects \
                if is_renderable(obj)]
        self.selected_cameras = [obj for obj in bpy.context.selected_objects \
                if obj.type == 'CAMERA']

        self.got_data = True
        if not self.sanity_check():
            return {'CANCELLED'}


    def invoke(self, context: bpy.types.Context, event: bpy.types.Event):
        data = self.get_data(context)
        if data:
            return data

        self.execute(context)
        return {'FINISHED'}

    def error_warning(self, error_message: str):
        self.report({'WARNING'}, error_message)
        print("\n", error_message)
        return False

    def sanity_check(self):

        render_basepath = get_render_basepath()
        if not check_dir(render_basepath):
            error_message = f'Can\'t find output path folder: {render_basepath}'
            return self.error_warning(error_message)

        if len(self.selected_cameras) < 1:
            error_message = "At least one camera has to be selected"
            return self.error_warning(error_message)

        for camera in self.selected_cameras:
            cam_settings = camera.data.prj_drawing_settings

            for name in cam_settings.back_subjects.split(','):
                if not name:
                    continue
                if name.strip():
                    continue
                if name.strip() in bpy.data.objects:
                    continue
                error_message = f'{name.strip()} object is missing'
                return self.error_warning(error_message)

            if filename_re.search(cam_settings.drawing_name.strip()):
                ## Drawing name contains non-alphanumeric characters
                error_message = 'Drawing name contains forbidden characters'
                return self.error_warning(error_message)


        return True

classes = (Prj,)

def register():
    for cls in classes:
        bpy.utils.register_class(cls)

def unregister():
    if prj.preferences.dependencies_installed:
        for cls in classes:
            bpy.utils.unregister_class(cls)

if __name__ == "__main__":
    register()
