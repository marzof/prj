import bpy
import math
import re
import gpu
from pathlib import Path as Filepath
from gpu_extras.batch import batch_for_shader
from mathutils import Vector
from prj.utils import is_renderable, cam_frame_factors, debug_here
from prj.utils import get_addon_preferences
from prj.preferences import PRJ_OT_install_dependencies, get_deps_installation
import prj.drawing_limits

SCALE_DEFAULT_VALUE = 100
SCALE_CUSTOM_VALUE = 'CUSTOM'
SCALES = [
        ('10000', '1:10000', '', 10000),
        ('5000', '1:5000', '', 5000),
        ('2000', '1:2000', '', 2000),
        ('1000', '1:1000', '', 1000),
        ('500', '1:500', '', 500),
        ('200', '1:200', '', 200),
        ('100', '1:100', '', 100),
        ('50', '1:50', '', 50),
        ('20', '1:20', '', 20),
        ('10', '1:10', '', 10),
        ('5', '1:5', '', 5),
        ('2', '1:2', '', 2),
        ('1', '1:1', '', 1), 
        (SCALE_CUSTOM_VALUE, 'Custom', 'Custom scale', 0), ]
LINKING = [
        ('linked', 'Linked', '', 0),
        ('embedded', 'Embedded', '', 1),]
symbol_type_descriptions = {
        'general': 'Type of drawing symbol (drawn by cameras whose Z ' + \
                'axis is parallel to object\'s Z axis)',
        'cut_line': 'Type of drawing symbol (drawn by cameras whose Z ' + \
                'axis is parallel to object\'s Z axis)',
        'stairs_cut': 'Stairs cut line (only firts and last verts are ' + \
                'considered. Stairs parts on origin side are kept)',
                }
copied_obj_prj_cam = None

def update_custom_scale(self, context) -> None:
    """ Keep camera custom scale drawing_setting updated """
    settings = self.id_data.prj_drawing_settings
    if settings.drawing_scale == SCALE_CUSTOM_VALUE:
        return
    settings.scale_numerator = 1.0
    settings.scale_denominator = float(settings.drawing_scale)

def update_drawing_limits(self, context) -> None:
    """ Update camera drawing limits """
    obj = context.active_object
    if not obj or not obj.data == self.id_data:
        return
    bpy.ops.prj.drawing_limits('INVOKE_DEFAULT', camera_name=self.id_data.name, 
            startup=False)

def get_install_deps_docs(layout):
    lines = [f"Please install the missing dependencies for the prj add-on.",
            f"1. Open the preferences (Edit > Preferences > Add-ons).",
            f"2. Search for the \"prj add-on.",
            f"3. Open the details section of the add-on.",
            f"4. Click on the {PRJ_OT_install_dependencies.bl_label}\" button.",
            f"   This will download and install the missing Python " + \
                    "packages, if Blender has the required permissions."]
    for line in lines:
        layout.label(text=line)

def set_camera(self, context) -> None:
    """ Assign object camera to slot and keep active settings """
    ## TODO fix "Failed to set value" warning message when camera is selected
    if not self:
        return
    obj = self.id_data
    cam = self.prj_selected_camera
    existing_cams = [cam.name for cam in obj.prj_camera]
    if not cam or cam.type != 'CAMERA' or cam.name in existing_cams:
        self.prj_selected_camera = None
        return
    cam_id = self.prj_active_camera
    self.prj_camera[cam_id].name = cam.name
    for a in PRJ_obj_cam_props.__annotations__:
        if 'BoolProperty' not in str(PRJ_obj_cam_props.__annotations__[a]):
            continue
        obj.prj_camera[cam_id][a] = obj.prj_drawing_settings[a]
    self.prj_selected_camera = None

def update_drawing_settings(self, context) -> None:
    """ Show drawing settings for active camera """
    cam_id = self.prj_active_camera
    obj = self.id_data
    if len(obj.prj_camera) == 0:
        return
    for a in PRJ_obj_cam_props.__annotations__:
        if 'BoolProperty' not in str(PRJ_obj_cam_props.__annotations__[a]):
            continue
        if a not in self.prj_camera[cam_id]:
            self.prj_camera[cam_id][a] = False
        self.prj_drawing_settings[a] = self.prj_camera[cam_id][a]

def add_object_camera(obj) -> None:
    """ Add camera to obj """
    obj.prj_camera.add()

def get_obj_and_cam_id(caller):
    obj = caller.id_data
    if len(obj.prj_camera) == 0:
        add_object_camera(obj)
    cam_id = obj.prj_active_camera
    return obj, cam_id

def get_xray(self) -> bool:
    """ Get xray value for active prj_camera """
    obj, cam_id = get_obj_and_cam_id(self)
    if "xray" not in self:
        self["xray"] = obj.prj_camera[cam_id].xray
    return self["xray"]

def get_cut_only(self) -> bool:
    """ Get cut_only value for active prj_camera """
    obj, cam_id = get_obj_and_cam_id(self)
    if "cut_only" not in self:
        self["cut_only"] = obj.prj_camera[cam_id].cut_only
    return self["cut_only"]

def get_hide_prj(self) -> bool:
    """ Get hide_projected value for active prj_camera """
    obj, cam_id = get_obj_and_cam_id(self)
    if "hide_projected" not in self:
        self["hide_projected"] = obj.prj_camera[cam_id].hide_projected
    return self["hide_projected"]

def get_outline(self) -> bool:
    """ Get outline value for active prj_camera """
    obj, cam_id = get_obj_and_cam_id(self)
    if "outline" not in self:
        self["outline"] = obj.prj_camera[cam_id].outline
    return self["outline"]

def get_hidden(self) -> bool:
    """ Get hidden value for active prj_camera """
    obj, cam_id = get_obj_and_cam_id(self)
    if "hidden" not in self:
        self["hidden"] = obj.prj_camera[cam_id].hidden
    return self["hidden"]

def get_force(self) -> bool:
    """ Get force value for active prj_camera """
    obj, cam_id = get_obj_and_cam_id(self)
    if "force" not in self:
        self["force"] = obj.prj_camera[cam_id].force
    return self["force"]

def get_ignore(self) -> bool:
    """ Get ignore value for active prj_camera """
    obj, cam_id = get_obj_and_cam_id(self)
    if "ignore" not in self:
        self["ignore"] = obj.prj_camera[cam_id].ignore
    return self["ignore"]

def set_xray(self, value) -> None:
    """ Set xray value to object for active prj_camera """
    obj, cam_id = get_obj_and_cam_id(self)
    obj.prj_camera[cam_id].xray = value
    self["xray"] = value

def set_cut_only(self, value) -> None:
    """ Set cut_only value to object for active prj_camera """
    obj, cam_id = get_obj_and_cam_id(self)
    obj.prj_camera[cam_id].cut_only = value
    self["cut_only"] = value

def set_hide_projected(self, value) -> None:
    """ Set hide_projected value to object for active prj_camera """
    obj, cam_id = get_obj_and_cam_id(self)
    obj.prj_camera[cam_id].hide_projected = value
    self["hide_projected"] = value

def set_outline(self, value) -> None:
    """ Set outline value to object for active prj_camera """
    obj, cam_id = get_obj_and_cam_id(self)
    obj.prj_camera[cam_id].outline = value
    self["outline"] = value

def set_hidden(self, value) -> None:
    """ Set hidden value to object for active prj_camera """
    obj, cam_id = get_obj_and_cam_id(self)
    obj.prj_camera[cam_id].hidden = value
    self["hidden"] = value

def set_force(self, value) -> None:
    """ Set force value to object for active prj_camera """
    obj, cam_id = get_obj_and_cam_id(self)
    obj.prj_camera[cam_id].force = value
    self["force"] = value

def set_ignore(self, value) -> None:
    """ Set ignore value to object for active prj_camera """
    obj, cam_id = get_obj_and_cam_id(self)
    obj.prj_camera[cam_id].ignore = value
    self["ignore"] = value


set_prop_dict = {
        'xray': set_xray,
        'cut_only': set_cut_only,
        'hide_projected': set_hide_projected,
        'outline': set_outline,
        'hidden': set_hidden,
        'force': set_force,
        'ignore': set_ignore,
        }


class PRJ_obj_settings(bpy.types.PropertyGroup):
    xray: bpy.props.BoolProperty(name='prj_xray_drawing',
            description='Draw the object as if not occluded and not ' + \
                    'occluding other object',
            set=set_prop_dict['xray'], get=get_xray,
            override={'LIBRARY_OVERRIDABLE'})
    cut_only: bpy.props.BoolProperty(name='prj_cut_only_drawing',
            description='Draw just the cut of the object',
            set=set_prop_dict['cut_only'], get=get_cut_only,
            override={'LIBRARY_OVERRIDABLE'})
    hide_projected: bpy.props.BoolProperty(name='prj_hide_projected_drawing',
            description='Don\'t draw visible parts of object',
            set=set_prop_dict['hide_projected'], get=get_hide_prj,
            override={'LIBRARY_OVERRIDABLE'})
    outline: bpy.props.BoolProperty(name='prj_outline_drawing', 
            description='Just draw the object outline',
            set=set_prop_dict['outline'], get=get_outline,
            override={'LIBRARY_OVERRIDABLE'})
    hidden: bpy.props.BoolProperty(name='prj_hidden_drawing', 
            description='Draw hidden parts of object',
            set=set_prop_dict['hidden'], get=get_hidden,
            override={'LIBRARY_OVERRIDABLE'})
    force: bpy.props.BoolProperty(name='prj_force_drawing', 
            description='Force object drawing (it has to be visible anyway)',
            set=set_prop_dict['force'], get=get_force,
            override={'LIBRARY_OVERRIDABLE'})
    ignore: bpy.props.BoolProperty(name='prj_ignore_object', 
            description='Ignore object',
            set=set_prop_dict['ignore'], get=get_ignore,
            override={'LIBRARY_OVERRIDABLE'})
    symbol_type: bpy.props.EnumProperty(name='prj_symbol_type', 
        description=symbol_type_descriptions['general'],
        items=[
            ('NONE', 'None', 'Not a symbol', 0), 
            ('GENERIC', 'Generic symbol', 
                symbol_type_descriptions['general'], 1),
            ('CUT_LINE', 'Cut line',
                symbol_type_descriptions['cut_line'], 2),
            ('STAIRS_CUT', 'Stairs cut line', 
                symbol_type_descriptions['stairs_cut'], 3), ],
            override={'LIBRARY_OVERRIDABLE'})
    object_description: bpy.props.StringProperty(
            name='prj_object_description', 
            description='Description of object', 
            override={'LIBRARY_OVERRIDABLE'})
    object_tags: bpy.props.StringProperty(
            name='prj_object_tags', 
            description='Objects tags', 
            override={'LIBRARY_OVERRIDABLE'})

class PRJ_add_back_subjects(bpy.types.Operator):
    bl_idname = "prj.add_back_subjects"
    bl_label = "Add objects to draw even if behind the camera"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        """ Add object prj_camera """
        cam = context.active_object

        back_subjects = []
        for name in cam.data.prj_drawing_settings.back_subjects.split(','):
            if not name:
                continue
            back_subjects.append(name.strip())

        for obj in context.selected_objects:
            if obj == cam:
                continue
            if not is_renderable(obj):
                continue
            if obj.name in back_subjects:
                continue
            back_subjects.append(obj.name)

        cam.data.prj_drawing_settings.back_subjects = ', '.join(back_subjects)
        return {'FINISHED'}


class PRJ_obj_settings_copy(bpy.types.Operator):
    bl_idname = "prj.obj_settings_copy"
    bl_label = "Copy prj setting from active object camera"
    bl_description = "Copy prj settings from active object camera"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        global copied_obj_prj_cam
        obj = context.active_object
        obj_cam = obj.prj_camera[obj.prj_active_camera]
        copied_obj_prj_cam = obj_cam
        return {'FINISHED'}


class PRJ_obj_settings_paste(bpy.types.Operator):
    bl_idname = "prj.obj_settings_paste"
    bl_label = "Paste prj settings to active object camera"
    bl_description = "Paste prj settings to active object camera"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        return copied_obj_prj_cam

    def execute(self, context):
        obj = context.active_object
        obj_cam = obj.prj_camera[obj.prj_active_camera]
        for ann in copied_obj_prj_cam.__annotations__:
            if ann == 'name':
                continue
            set_prop_dict[ann](obj.prj_drawing_settings, copied_obj_prj_cam[ann])
        return {'FINISHED'}

class PRJ_obj_settings_copy_to_selected(bpy.types.Operator):
    bl_idname = "prj.obj_settings_copy_to_selected"
    bl_label = "Copy From Active Object to selected objects"
    bl_description = "Copy PRJ Object settings from active to all selected"
    bl_options = {'REGISTER', 'UNDO'}

    @classmethod
    def poll(cls, context):
        return len(context.selected_objects) > 1 \
                and context.active_object is not None

    def execute(self, context):
        active = context.active_object
        for obj in context.selected_objects:
            if obj == active:
                continue
            for ann in PRJ_obj_settings.__annotations__:
                setattr(obj.prj_drawing_settings, ann, 
                        getattr(active.prj_drawing_settings, ann))
            obj.prj_camera.clear()
            for cam in active.prj_camera:
                new = obj.prj_camera.add()
                new.name = cam.name
                for ann in PRJ_obj_cam_props.__annotations__:
                    setattr(new, ann, getattr(cam, ann))
        return {'FINISHED'}

class PRJ_obj_cam_props(bpy.types.PropertyGroup):
    """ Object prj_camera properties """
    name: bpy.props.StringProperty(name="name")
    xray: bpy.props.BoolProperty(name="xray")
    cut_only: bpy.props.BoolProperty(name="cut_only")
    hide_projected: bpy.props.BoolProperty(name="hide")
    outline: bpy.props.BoolProperty(name="outline")
    hidden: bpy.props.BoolProperty(name="hidden")
    force: bpy.props.BoolProperty(name="force")
    ignore: bpy.props.BoolProperty(name="ignore")

class PRJ_add_obj_cam(bpy.types.Operator):
    bl_idname = "prj.add_obj_cam"
    bl_label = "Add object camera"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        """ Add object prj_camera """
        obj = context.object
        obj.prj_camera.add()
        obj.prj_active_camera += 1
        return {'FINISHED'}

class PRJ_remove_obj_cam(bpy.types.Operator):
    bl_idname = "prj.remove_obj_cam"
    bl_label = "Remove object camera"
    bl_options = {'REGISTER', 'UNDO'}

    def execute(self, context):
        """ Remove object prj_camera """
        obj = context.object
        cam = obj.prj_active_camera
        obj.prj_camera.remove(cam)
        active_cam = obj.prj_active_camera
        obj.prj_active_camera = active_cam - 1 if active_cam > 0 else 0
        return {'FINISHED'}

class PRJ_cam_settings(bpy.types.PropertyGroup):
    drawing_scale: bpy.props.EnumProperty(name='prj_scale', 
            description='Scale of drawing', default=SCALE_DEFAULT_VALUE,
            items=SCALES, override={'LIBRARY_OVERRIDABLE'}, 
            update=update_custom_scale)
    scale_numerator: bpy.props.FloatProperty(
            name='prj_scale_num', description='Scale numerator', 
            default=1.0, min=.001, options={'HIDDEN'},
            override={'LIBRARY_OVERRIDABLE'})
    scale_denominator: bpy.props.FloatProperty(
            name='prj_scale_den', description='Scale denominator', 
            default=SCALE_DEFAULT_VALUE, min=.001, options={'HIDDEN'},
            override={'LIBRARY_OVERRIDABLE'})
    drawing_ratio: bpy.props.FloatProperty(
            name='prj_drawing_ratio', description='Width/height', 
            default=1, min=0, max=2**31-1, soft_min=0, #default=math.sqrt(2)
            soft_max=2**31-1, override={'LIBRARY_OVERRIDABLE'}, 
            update=update_drawing_limits)
    view_symbol: bpy.props.FloatProperty(
            name='prj_view_symbol', 
            description='Symbols are skipped beyond this distance from camera', 
            default=10.0, min=0, max=2**31-1, soft_min=0, soft_max=2**31-1,
            override={'LIBRARY_OVERRIDABLE'})
    level_of_detail: bpy.props.IntProperty(
            name='prj_LOD', description='Level of detail', 
            default=1, min=0, max=2, soft_min=0, soft_max=2,
            override={'LIBRARY_OVERRIDABLE'})
    back_subjects: bpy.props.StringProperty(
            name='prj_back_subjects', 
            description='Objects to back draw for this camera. ' + \
                    'Separate objects by comma (,)', 
            override={'LIBRARY_OVERRIDABLE'})
            ## TODO back subjects names are created in depsgraph: 
            ##      find a way to get them or use PointerProperty and slots
    drawing_name: bpy.props.StringProperty(
            name='prj_drawing_name', 
            description='Drawing name',
            override={'LIBRARY_OVERRIDABLE'})
    disable_render: bpy.props.BoolProperty(
            name='prj_disable_render', 
            description='Disable accurate visibility calculation by rendering',
            default=False,
            override={'LIBRARY_OVERRIDABLE'})
    export_svg: bpy.props.BoolProperty(
            name='prj_export_svg', 
            description='Export drawing in svg format',
            default=True,
            override={'LIBRARY_OVERRIDABLE'})
    export_dxf: bpy.props.BoolProperty(
            name='prj_export_dxf', 
            description='Export drawing in dxf format',
            override={'LIBRARY_OVERRIDABLE'})
    export_dwg: bpy.props.BoolProperty(
            name='prj_export_dwg', 
            description='Export drawing in dwg format',
            override={'LIBRARY_OVERRIDABLE'})
    linking_type: bpy.props.EnumProperty(name='prj_linking_type', 
            description='Type of files connection', default='linked',
            items=LINKING, override={'LIBRARY_OVERRIDABLE'})

class PRJ_MT_obj_settings_menu(bpy.types.Menu):
    bl_idname = "PRJ_MT_obj_settings_menu"
    bl_label = "Select"

    def draw(self, context):
        layout = self.layout
        layout.operator('prj.obj_settings_copy', text='Copy settings', 
                icon='COPYDOWN')
        layout.operator("prj.obj_settings_copy_to_selected", 
                text="Copy prj settings to selected")
        layout.operator('prj.obj_settings_paste', text='Paste settings', 
                icon='PASTEDOWN')

early_classes = [
        PRJ_obj_settings, 
        PRJ_obj_cam_props, 
        PRJ_cam_settings, 
        PRJ_add_obj_cam, 
        PRJ_remove_obj_cam,
        PRJ_add_back_subjects,
        PRJ_MT_obj_settings_menu,
        ]
for cls in early_classes:
    bpy.utils.register_class(cls)

bpy.types.Object.prj_drawing_settings = bpy.props.PointerProperty(
        type=PRJ_obj_settings, override={'LIBRARY_OVERRIDABLE'})
bpy.types.Object.prj_camera = bpy.props.CollectionProperty(
        type=PRJ_obj_cam_props, options={'LIBRARY_EDITABLE'},
        override={'LIBRARY_OVERRIDABLE', 'USE_INSERTION'},)
bpy.types.Object.prj_active_camera = bpy.props.IntProperty(
        update=update_drawing_settings, 
        override={'LIBRARY_OVERRIDABLE'})
bpy.types.Object.prj_selected_camera = bpy.props.PointerProperty(
        type=bpy.types.Object, update=set_camera, 
        poll=lambda self, object: object.type=="CAMERA",
        override={'LIBRARY_OVERRIDABLE'})
bpy.types.Camera.prj_drawing_settings = bpy.props.PointerProperty(
        type=PRJ_cam_settings, override={'LIBRARY_OVERRIDABLE'})

class OBJECT_UL_prj_camslots(bpy.types.UIList):
    def draw_item(self, context, layout, data, item, icon, active_data, 
            active_propname):
        ob = data
        obj_cam = item
        icons = bpy.types.UILayout.bl_rna.functions['prop'].parameters['icon']
        for ico in icons.enum_items:
            if ico.name != 'CAMERA_DATA':
                continue
            icon = ico.value
            break

        if self.layout_type in {'DEFAULT', 'COMPACT'}:
            if ob:
                layout.prop(obj_cam, "name", text="", emboss=False, 
                        icon_value=icon)
            else:
                layout.label(text="", translate=False, icon_value=icon)
        elif self.layout_type in {'GRID'}:
            layout.alignment = 'CENTER'
            layout.label(text="", icon_value=icon)

class OBJECT_PT_prj_settings(bpy.types.Panel):
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_idname = "OBJECT_PT_ui_prj_settings"
    bl_context = "object"
    bl_label = "Prj settings"

    @classmethod
    def poll(cls, context):
        return (is_renderable(context.object))

    def draw(self, context):
        layout = self.layout
        if not get_deps_installation():
            get_install_deps_docs(layout)
            return
        layout.use_property_split = True
        obj = context.object
        obj_type = obj.type
        is_drawable = (obj_type in {'MESH', 'CURVE', 'EMPTY'})
        if is_drawable:
            row = layout.row()
            row.template_list("OBJECT_UL_prj_camslots", "", obj,
                    "prj_camera", obj, "prj_active_camera")
            col = row.column(align=True)
            col.operator("prj.add_obj_cam", icon='ADD', text="")
            if obj.prj_active_camera != 0:
                col.operator("prj.remove_obj_cam", icon='REMOVE', text="")
            col.separator()
            col.menu("PRJ_MT_obj_settings_menu", icon='DOWNARROW_HLT', text="")
            row = layout.row()
            if obj.prj_active_camera != 0:
                row.prop(obj, "prj_selected_camera", 
                        text="Assign camera to slot")
            col = layout.column(heading="Options")
            col.prop(obj.prj_drawing_settings, "xray", text="Xray")
            col.prop(obj.prj_drawing_settings, "cut_only", text="Cut only")
            col.prop(obj.prj_drawing_settings, "hide_projected", 
                    text="Hide projected")
            col.prop(obj.prj_drawing_settings, "outline", text="Outline")
            col.prop(obj.prj_drawing_settings, "hidden", text="Hidden")
            col.prop(obj.prj_drawing_settings, "force", text="Force")
            col.prop(obj.prj_drawing_settings, "ignore", text="Ignore")
            col.prop(obj.prj_drawing_settings, "symbol_type", text="Symbol type")
            col.prop(obj.prj_drawing_settings, "object_description", 
                    text="Description")
            col.prop(obj.prj_drawing_settings, "object_tags", text="Tags")

class DATA_PT_prj_cam_settings(bpy.types.Panel):
    bl_space_type = 'PROPERTIES'
    bl_region_type = 'WINDOW'
    bl_context = "data"
    bl_label = "Prj settings"

    @classmethod
    def poll(cls, context):
        return (context.object.type == 'CAMERA')

    def draw(self, context):
        layout = self.layout
        if not get_deps_installation():
            get_install_deps_docs(layout)
            return 
        layout.use_property_split = True
        obj = context.object
        settings = obj.data.prj_drawing_settings
        if obj.type == 'CAMERA' and obj.data.type == 'ORTHO':
            addon_preferences = get_addon_preferences()
            oda_fc_path = Filepath(addon_preferences.oda_fc_path.strip('"'))

            col = layout.column()
            col.prop(settings, "export_svg", text="Export svg")
            col.prop(settings, "export_dxf", text="Export dxf")
            if oda_fc_path.is_file():
                col.prop(settings, "export_dwg", text="Export dwg")
            col.prop(settings, "disable_render", 
                    text="Disable visibility render")

            scale_col = col.column()
            scale_col.enabled = settings.export_svg
            scale_col.prop(settings, "drawing_scale", text="Scale")

            custom_scale_row = col.row()
            custom_scale_row.enabled = settings.drawing_scale == \
                    SCALE_CUSTOM_VALUE and settings.export_svg
            custom_scale_row.prop(settings, "scale_numerator", 
                    text="Custom scale")
            custom_scale_row.prop(settings, "scale_denominator", text=":")

            lod_col = col.column()
            lod_col.enabled = not settings.disable_render
            lod_col.prop(settings, "level_of_detail", text="LOD")

            limits_row = col.row(align=True)
            limits_row.prop(settings, "drawing_ratio", text="Drawing limits")
            icon='RESTRICT_VIEW_OFF' if prj.drawing_limits.on \
                    else 'RESTRICT_VIEW_ON'
            drawing_limits_op = limits_row.operator("prj.drawing_limits", 
                    icon=icon, text='')
            if context.object:
                active_obj = context.active_object
                if active_obj.type == 'CAMERA':
                    camera_name = context.active_object.data.name
                    drawing_limits_op.camera_name = camera_name
                    drawing_limits_op.startup = True

            col.prop(settings, "view_symbol", text="View symbol")

            back_row = col.row(align=True)
            back_row.prop(settings, "back_subjects", text="Back subjects")
            back_row.operator("prj.add_back_subjects", icon='ADD', text="")

            col.prop(settings, "drawing_name", text="Drawing name")
            col.prop(settings, "linking_type", text="Linking type")
            col.operator("prj.prj", text="Draw it!")
        if obj.data.type != 'ORTHO':
            layout.label(text="Only available in Orthographic mode", icon="QUESTION")
            layout.prop(obj.data, "type")


classes = [OBJECT_PT_prj_settings, DATA_PT_prj_cam_settings, 
        OBJECT_UL_prj_camslots, PRJ_obj_settings_copy, PRJ_obj_settings_paste,
        PRJ_obj_settings_copy_to_selected,
        ]

def register():
    for cls in classes:
        bpy.utils.register_class(cls)

def unregister():
    for cls in early_classes:
        bpy.utils.register_class(cls)
    for cls in classes:
        bpy.utils.unregister_class(cls)

if __name__ == "prj.ui":
    register()
