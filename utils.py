#!/usr/bin/env python3.9
# -*- coding: utf-8 -*- 

import bpy, bmesh
import os
import re
import tempfile
from pathlib import Path
import math
import itertools
import prj
from mathutils import Matrix, Vector, geometry

re_bl_rel = r'\d+\.\d'
blender_release = bpy.app.version_string
BLENDER_VERSION = float(re.search(re_bl_rel, blender_release).group(0))
HI_RES_RENDER_FACTOR: int = 2
tmp_imgs = []
hi_res_percent: float = 1.0 ## Changed by set_hi_res_percent()
f_to_8_bit = lambda c: int(hex(int(c * 255)),0)
is_renderable = lambda obj: (obj.type, bool(obj.instance_collection)) \
        in [('MESH', False), ('CURVE', False), ('EMPTY', True)]
digits_re = re.compile('\d*')
ADDON = bpy.context.preferences.addons.get(prj.__name__)

get_render_basepath = lambda: bpy.path.abspath(bpy.context.scene.render.filepath)
check_dir = lambda path: True if Path(path).is_dir() else False

meter_inch_ratio = 39.3700787402
UNITS_SYSTEM = {
        'METRIC': {'svg_factor': 1., 'svg_unit': 'mm', 
            'svg_base_units': 'MILLIMETERS'},
        'IMPERIAL': {'svg_factor': meter_inch_ratio/1000., 'svg_unit': 'in',
            'svg_base_units': 'INCHES'}}
UNITS = {'KILOMETERS': 1000., 
        'METERS': 1., 
        'CENTIMETERS': 1./100., 
        'MILLIMETERS': 1./1000., 
        'MICROMETERS': 1./1000000., 
        'MILES': 63360./meter_inch_ratio,
        'FEET': 12./meter_inch_ratio, 
        'INCHES': 1./meter_inch_ratio, 
        'THOU': .001/meter_inch_ratio, 
        }

def get_drawing_styles():
    from prj.drawing_style import get_drawing_styles
    return get_drawing_styles()

def get_addon_preferences():
    if ADDON and hasattr(ADDON, "preferences"):
        return ADDON.preferences
    return None

def remove_file(filepath) -> None:
    """ Remove file in filepath """
    try:
        os.remove(filepath)
    except FileNotFoundError:
        pass

def get_prj_camera_id(obj: bpy.types.Object, cam_name: str) -> int:
    """ Get the id of obj prj_camera if consistent with cam_name """
    prj_camera_id = 0
    for i, prj_cam in enumerate(obj.prj_camera):
        if i == 0:
            continue
        if prj_cam.name != cam_name:
            continue
        prj_camera_id = i
        break
    return prj_camera_id

def debug_here(filepath="/home/mf/Documents/test_prj.blend") -> None:
    bpy.ops.wm.save_as_mainfile(filepath=filepath, copy=True)
    raise Exception('STOP')

def apply_modifier(obj: bpy.types.Object, mod: bpy.types.Modifier) \
        -> bpy.types.Object:
    """ Create a new object with modifier applied """
    ## Make sure that obj is visible in order to update depsgraph
    obj_visibility = obj.hide_viewport
    obj.hide_viewport = False
    depsgraph = bpy.context.evaluated_depsgraph_get()
    obj.hide_viewport = obj_visibility
    
    obj_eval = obj.evaluated_get(depsgraph)
    new_mesh = bpy.data.meshes.new_from_object(obj_eval)
    new_obj = bpy.data.objects.new(name='tmp_' + obj.name, 
            object_data=new_mesh)
    new_obj.matrix_world = obj.matrix_world
    return new_obj

def set_hi_res_percent(camera_data: bpy.types.Camera) -> float: 
    """ Get the percentage factor based on camera_data level of detail 
        and resolution_base """
    global hi_res_percent
    lod = camera_data.prj_drawing_settings.level_of_detail 
    hi_res_percent = HI_RES_RENDER_FACTOR ** lod
    return hi_res_percent

def get_hi_res_percent() -> float:
    return hi_res_percent

def cam_frame_factors(cam_data: bpy.types.Camera) -> dict[str, float]:
    """ Calculate x and y factors for cam considering drawing ratio """
    x_factor = 1 * min(1, cam_data.prj_drawing_settings.drawing_ratio)
    y_factor = 1 / max(1, cam_data.prj_drawing_settings.drawing_ratio)
    return {'x': x_factor, 'y': y_factor}

def ranges(ints: list[int]):
    for a, b in itertools.groupby(enumerate(ints), 
            lambda pair: pair[1] - pair[0]):
        b = list(b)
        yield b[0][1], b[-1][1]

def create_grease_pencil(name: str) -> bpy.types.Object:
    """ Create a grease pencil """
    gp = bpy.data.grease_pencils.new(name)

    gp_layer = gp.layers.new(name + '_gp_layer')
    gp_layer.frames.new(1)
    
    gp_mat = bpy.data.materials.new(name + '_gp_material')
    bpy.data.materials.create_gpencil_data(gp_mat)
    gp.materials.append(gp_mat)

    obj = bpy.data.objects.new(name, gp)
    obj.show_in_front = True
    return obj

def get_object_collections(obj: bpy.types.Object, 
        scene_tree: dict[tuple[int], bpy.types.Collection]) \
                -> list[bpy.types.Collection]:
    """ Get the collections obj belongs to (based on scene_tree)"""
    obj_collections = []
    if not obj:
        return obj_collections
    if not scene_tree:
        return obj_collections
    for position in scene_tree:
        if scene_tree[position] != obj:
            continue
        ## For obj in position like (0, 0, 2, 1, 0, 1) get collections at 
        ##      position: (0,0), (0,0,2), (0,0,2,1), (0,0,2,1,0)
        for i in range(1, len(position)):
            if type(scene_tree[position[:i]]) != bpy.types.Collection:
                continue
            collection = scene_tree[position[:i]]
            obj_collections.append(scene_tree[position[:i]])
    return obj_collections

def name_cleaner(name: str) -> str:
    return re.sub(r'[^\w\d-]', '_', name, flags=re.IGNORECASE)

def add_modifier(obj: bpy.types.Object, mod_name: str, mod_type: str, 
        params: dict, gpencil: bool= False) -> bpy.types.Modifier:
    if gpencil:
        mod = obj.grease_pencil_modifiers.new(mod_name, mod_type)
    else:
        mod = obj.modifiers.new(mod_name, mod_type)
    for param in params:
        setattr(mod, param, params[param])
    return mod

def to_hex(c: float) -> str:
    """ Return srgb hexadecimal version of c """
    if c < 0.0031308:
        srgb = 0.0 if c < 0.0 else c * 12.92
    else:
        srgb = 1.055 * math.pow(c, 1.0 / 2.4) - 0.055
    return hex(max(min(int(srgb * 255 + 0.5), 255), 0))

def unfold_ranges(ranges: list[tuple[int]]) -> list[int]:
    """ Flatten ranges in a single list of value """
    result = []
    for r in ranges:
        if len(r) == 1:
            result += [r[0]]
            continue
        result += list(range(r[0], r[1]+1))
    return result

class dotdict(dict):
    """dot.notation access to dictionary attributes"""
    __getattr__ = dict.get
    __setattr__ = dict.__setitem__
    __delattr__ = dict.__delitem__

def get_resolution(camera_data: bpy.types.Camera, drawing_scale: float = None) \
        -> dict[str,list[float]]:
    """ Get scene render resolution based on drawing_scale 
        (return float values if raw is True) """
    if not drawing_scale:
        settings = camera_data.prj_drawing_settings
        drawing_scale = settings.scale_numerator / settings.scale_denominator
    scale_factor = drawing_scale
    factor_x = cam_frame_factors(camera_data)['x']
    factor_y = cam_frame_factors(camera_data)['y']
    ## ortho_scale are expressed in meter, no matter what units are set,
    ## multiply by 1000 and convert it to millimeters 
    ## in order to get a reference for pixels
    x_size = scale_factor * round(camera_data.ortho_scale * 1000, 5) * factor_x
    y_size = scale_factor * round(camera_data.ortho_scale * 1000, 5) * factor_y
    raw_resolution = [x_size, y_size]
    resolution = [int(round(v)) for v in [x_size, y_size]]
    return {'resolution': resolution, 'raw': raw_resolution}

def get_render_data(objects: list[bpy.types.Object], 
        scene: bpy.types.Scene) -> 'Imaging_Core':
    """ Render the objects and get the pixels data """
    from PIL import Image
    global tmp_imgs
    ### Move objects to scene, render them and remove from scene
    moved_objs = []
    for obj in objects:
        if obj not in list(scene.collection.objects):
            scene.collection.objects.link(obj)
            moved_objs.append(obj)
    original_render_filepath = scene.render.filepath
    fd, tmp_file = tempfile.mkstemp(suffix='.tif')
    scene.render.filepath = tmp_file
    with open(tmp_file, 'wb') as f:
        bpy.ops.render.render(write_still=True, scene=scene.name)
    for obj in moved_objs:
        if obj in list(scene.collection.all_objects):
            scene.collection.objects.unlink(obj)

    ### Get the rendering data
    with Image.open(tmp_file) as render:
        render_pixels = list(render.getdata())
    os.close(fd)
    tmp_imgs.append(tmp_file)
    scene.render.filepath = original_render_filepath
    return render_pixels

def flatten(li: list) -> list:
    """ Flatten list li from its sublists """
    return [item for sublist in li for item in sublist]

def get_scene_resolution(scene: bpy.types.Scene) -> list[int]:
    return [bpy.context.scene.render.resolution_x,
            bpy.context.scene.render.resolution_y]

def set_scene_resolution(scene: bpy.types.Scene, resolution: list[int]) -> None:
    bpy.context.scene.render.resolution_x = resolution[0]
    bpy.context.scene.render.resolution_y = resolution[1]

def make_active(obj: bpy.types.Object, scene: bpy.types.Scene = None) -> None:
    """ Deselect all and make obj active """
    if not scene:
        scene = bpy.context.scene
    for o in bpy.context.selected_objects:
        o.select_set(False)
    obj.select_set(True, view_layer=scene.view_layers[0])
    bpy.context.window.view_layer.objects.active = obj

