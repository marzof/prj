#!/usr/bin/env python3.9
# -*- coding: utf-8 -*- 

# Copyright (c) 2021 Marco Ferrara

# License:
# GNU GPL License
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies: 
# TODO...

import bpy
from mathutils import Vector
from bpy_extras.object_utils import world_to_camera_view
from prj.scene_tree import Scene_tree
from prj.working_scene import get_working_scene
from prj.drawing_subject import Drawing_subject 
from prj.drawing_camera import get_drawing_camera
from prj.geometry_utils import is_parallel_to_plane, framed_box
from prj.geometry_utils import get_bounding_rect, get_framed_rect
from prj.utils import get_object_collections, get_prj_camera_id, debug_here
from prj.progress_indicator import get_progress_indicator

library_trees = {}

def is_visible(obj: bpy.types.Object, 
        obj_collections: list[bpy.types.Collection]) -> bool: 
    """ Return False if obj's view is disabled by any type of hiding """
    ## TODO check collections visibility too
    return not obj.hide_render and not obj.hide_viewport and not obj.hide_get()

def is_legal_symbol(obj: bpy.types.Object, matrix: 'mathutils.Matrix', 
        drawing_camera: 'Drawing_camera', scene: bpy.types.Scene = None) -> bool:
    """ Check if symbol obj is parallel to camera and if it's in camera clip 
        range """
    cam = drawing_camera
    if not scene:
        scene = bpy.context.scene
    ## Z axis of obj and camera have to be parallel
    if not is_parallel_to_plane(obj, matrix, cam.direction):
        return False
    ## Filter too far or too close symbols 
    obj_loc = matrix.to_translation()
    cam_dist = world_to_camera_view(scene, cam.obj, obj_loc).z
    if cam_dist < cam.symbol_range[0] or cam_dist > cam.symbol_range[1]:
        return False
    return True

def get_linked_collection(filepath: str,
    linked_collection: bpy.types.Collection) -> bpy.types.Collection:
    """ Append linked_collection (and collections inside it) to the file """
    local_collection = bpy.data.collections[linked_collection.name, filepath]
    if local_collection:
        ## linked_collection is already loaded in the file
        return local_collection
    with bpy.data.libraries.load(filepath) as (data_from, data_to):
        for coll_name in data_from.collections:
            if coll_name == linked_collection.name:
                data_to.collections.append(coll_name)
                break
    collection = data_to.collections[0]
    return collection

def get_obj_scene_tree(instancer: bpy.types.Object, obj: bpy.types.Object = None, 
        lib_path: str = None) -> 'prj.Scene_tree':
    """ Get the obj scene tree either obj is local or linked """
    global library_trees
    if lib_path:
        ## obj comes from another file
        instanced_collection = instancer.instance_collection
        if instanced_collection.library.filepath != lib_path:
            ## obj is linked from a linked file: 
            ## get the right collection which belongs to
            for coll in bpy.data.collections:
                if not coll.library:
                    continue
                if coll.library.filepath != lib_path:
                    continue
                if obj.name not in coll.all_objects:
                    continue
                instanced_collection = coll
                break
        inst_collection_name = lib_path + '-' + instanced_collection.name
        if inst_collection_name not in library_trees:
            linked_collection = get_linked_collection(lib_path, 
                    instanced_collection)
            library_trees[inst_collection_name] = Scene_tree(
                    linked_collection).tree
        return library_trees[inst_collection_name]

    ## obj come from a scene in the same file
    for scene in bpy.data.scenes:
        if instancer not in list(scene.collection.all_objects):
            continue
        if scene.name not in library_trees:
            library_trees[scene.name] = Scene_tree(scene.collection).tree
        return library_trees[scene.name]

def get_symbol_type(obj):
    draw_settings = obj.prj_drawing_settings
    if draw_settings.symbol_type == 'NONE':
        return None
    return draw_settings.symbol_type 

def get_in_frame_subjects() -> list[Drawing_subject]:
    """ Check for all object instances in scene and return those which are 
        inside camera frame (and camera limits) as Drawing_subject(s)"""

    global library_trees
    depsgraph = bpy.context.evaluated_depsgraph_get()
    working_scene = get_working_scene()
    drawing_camera = get_drawing_camera()
    current_scene_tree = Scene_tree(bpy.context.scene.collection).tree
    library_trees[bpy.context.scene.name] = current_scene_tree
    framed_subjects = []

    ## Get back_subject
    back_subjects_string = drawing_camera.data.prj_drawing_settings.back_subjects
    if back_subjects_string:
        back_subjs: list[bpy.types.Object] = [bpy.data.objects[name.strip()] \
                for name in back_subjects_string.split(',')]
        ## Replace linked collections with its content
        back_subjects = []
        for back_subj in back_subjs:
            if back_subj.type != 'EMPTY':
                back_subjects.append(back_subj)
                continue
            if not back_subj.instance_collection:
                continue
            back_subjects += list(back_subj.instance_collection.all_objects)
    else:
        back_subjects = []
    print('back_subjs', back_subjects)

    progress = get_progress_indicator()
    progress.set_phase(phase_data={'name': 'get_in_frame_subjects',
                'start':progress.get_last_update(), 
                'end':progress.get_next_step('main'), 
                'steps':len(depsgraph.object_instances)})


    for i, obj_inst in enumerate(depsgraph.object_instances):
        progress.update(phase_name='get_in_frame_subjects', step=i)
        obj_eval = obj_inst.object
        obj = obj_eval.original

        if obj_inst.is_instance and obj_eval.data:
            ## Create an object for instanced object by parenting
            ## Exclude Empty objects
            print('obj_eval', obj_eval)
            mesh_from_eval = bpy.data.meshes.new_from_object(obj_eval)
            obj = bpy.data.objects.new(obj.name, mesh_from_eval) 
            working_scene.link_object(obj) 
        instancer = {'eval': obj_inst.parent, 
                'obj': None, 'draw_settings': None, 'symbol_type': None}
        print(f'Process {obj.name} (instanced by {instancer["eval"]})')

        ## Check if obj is a legal type
        if obj.type not in ['CURVE', 'MESH']:
            continue

        ## Get data about obj_inst
        if instancer['eval']:
            instancer['obj'] = instancer['eval'].original
            instancer['draw_settings'] = instancer['obj'].prj_drawing_settings
            instancer['symbol_type'] = get_symbol_type(instancer['obj'])

        ## Get obj.prj_camera id
        obj_cam_id = get_prj_camera_id(obj, drawing_camera.original_name)

        ## Not proceeding if object is ignored
        if obj.prj_camera and obj.prj_camera[obj_cam_id].ignore:
            continue
        if not obj.prj_camera and obj.prj_drawing_settings.ignore:
            continue
        if instancer['obj']:
            obj_cam_id = get_prj_camera_id(instancer['obj'], 
                    drawing_camera.original_name)
            if instancer['obj'].prj_camera and \
                    instancer['obj'].prj_camera[obj_cam_id].ignore:
                        continue
            if not instancer['obj'].prj_camera and \
                    instancer['obj'].prj_drawing_settings.ignore:
                        continue

        lib_path = obj.library.filepath if obj.library else obj.library
        if obj.override_library and obj.override_library.reference:
            over_lib_path = obj.override_library.reference.library.filepath
        else:
            over_lib_path = obj.override_library
        symbol_type = get_symbol_type(obj) 
        external_instanced = instancer['obj'] and (lib_path or over_lib_path)
        if external_instanced:
            obj_scene_tree = get_obj_scene_tree(instancer['obj'], obj, lib_path)
        else:
            obj_scene_tree = get_obj_scene_tree(obj)
        obj_collections = get_object_collections(obj, obj_scene_tree)
        ## Add instancer's collections in the current scene
        obj_collections += get_object_collections(instancer['obj'], 
                current_scene_tree)

        ## Not proceeding if object is not visible
        if not is_visible(obj, obj_collections):
            continue
        if instancer['obj'] and not is_visible(instancer['obj'], obj_collections):
            continue

        ## Check if obj_inst is inside camera frame
        matrix = obj_eval.matrix_world.copy()
        world_obj_bbox = [matrix @ Vector(v) for v in obj_eval.bound_box]
        obj_bbox_to_camera_view = [world_to_camera_view(working_scene.scene, 
            drawing_camera.obj, v) for v in world_obj_bbox]
        is_in_frame = framed_box(box=obj_bbox_to_camera_view, 
                frame=drawing_camera.normalized_frame)

        if not is_in_frame['result']:
            continue

        ## Filter not pertaining symbols
        if symbol_type or instancer['symbol_type']: 
            if obj.prj_camera and obj.prj_camera[obj_cam_id].force:
                pass
            elif not obj.prj_camera and obj.prj_drawing_settings.force:
                pass
            elif instancer['obj']:
                obj_cam_id = get_prj_camera_id(instancer['obj'], 
                    drawing_camera.original_name)
                if instancer['obj'].prj_camera and \
                        instancer['obj'].prj_camera[obj_cam_id].force:
                    pass
                elif not instancer['obj'].prj_camera and \
                        instancer['obj'].prj_drawing_settings.force:
                    pass
                elif not is_legal_symbol(instancer['obj'], matrix, 
                        drawing_camera, working_scene.scene): 
                    continue
            elif not is_legal_symbol(obj, matrix, drawing_camera, 
                    working_scene.scene): 
                continue

        ## Calculate the rectangular area of obj viewed from camera frame
        obj_rect = get_bounding_rect(obj_bbox_to_camera_view)
        frame_rect = get_bounding_rect(drawing_camera.normalized_frame)
        bounding_rect = get_framed_rect(obj_rect, frame_rect)

        ## Create the drawing_subject
        framed_subject = Drawing_subject(
                obj_eval=obj_eval,
                name=obj.name,
                mesh_eval = bpy.data.meshes.new_from_object(obj_eval),
                matrix=matrix.freeze(),
                instancer_eval=instancer['eval'],
                is_instance=obj_inst.is_instance,
                lib_path=lib_path,
                over_lib_path=over_lib_path,
                bounding_rect=bounding_rect,
                is_in_front=is_in_frame['in_front'], 
                is_behind=is_in_frame['behind'], 
                back_drawing = (bpy.data.objects[obj.name, lib_path] \
                        in back_subjects) or (instancer['obj'] in back_subjects),
                collections=obj_collections,
                symbol_type=symbol_type,
                instancer_symbol_type=instancer['symbol_type'],
                obj_cam_id = obj_cam_id,
                )
        framed_subjects.append(framed_subject)

    return framed_subjects

