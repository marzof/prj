#!/usr/bin/env python3.9
# -*- coding: utf-8 -*- 

# Copyright (c) 2021 Marco Ferrara

# License:
# GNU GPL License
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

# Dependencies: 
# TODO...

import bpy
import math
import numpy as np
from prj.utils import get_render_data

def get_colors_spectrum(size: int) -> list[tuple[float]]:
    """ Create enough different colors (rgb_values to the third power) to cover 
        size (at least) """
    rgb_values = math.ceil(size ** (1./3))
    spectrum = np.linspace(0, 1, rgb_values)
    colors = [(r, g, b, 1) for r in spectrum for g in spectrum for b in spectrum]
    return colors

def get_not_occluded_subjects(subjects: list['Drawing_subject'], 
        camera: bpy.types.Camera, working_scene: 'Working_scene') \
                -> list['Drawing_subject']:
    """ Render the working_scene at high resolution in order to get all visible 
        objects """

    ## Create colors and assign them to framed_subjects
    colors: list[tuple[float]] = get_colors_spectrum(len(subjects))
    subjs_to_hide = []
    for i, subj in enumerate(subjects):
        subj.set_color(colors[i])
        ## Wire and xray subjects have to be excluded from render in order 
        ## to not hide other subjects
        if subj.xray_drawing or subj.hidden_drawing:
            subj.obj.hide_render = True
            subjs_to_hide.append(subj)

    ## Execute a render in order to find out which objects are viewed (by colors)
    render_data = get_render_data([], working_scene.scene)
    viewed_colors = set(render_data)

    ## Reset visibility for xray and hidden subjects
    for subj in subjs_to_hide:
        subj.obj.hide_render = False

    visible_subjects = [subj for subj in subjects if subj.color in viewed_colors]
    ## Add xray and hidden subjects to visible subjects
    return visible_subjects + subjs_to_hide
